package com.parser.commonFunction;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.web.multipart.MultipartFile;

import com.parser.customError.CustomException;


public class CommonFunctionImpl {
	/**
	 * 
	 * @author Priti Kumari
	 * 
	 *      Used for adding nodes to a map object 	
	 *		   
	 */	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map getNodeObject(Object object) {
		
		 List<Object> li=null;		
		 Map<Object, Object> ipObject = new LinkedHashMap<>();
		 Map<Object, Object> tcpObject = new LinkedHashMap<>();
		 Map<Object, Object> udpObject = new LinkedHashMap<>();
		 Map<Object, Object> httpObject = new LinkedHashMap<>();
		 Map<Object, Object> sctpObject = new LinkedHashMap<>();
		 Map<Object, Object> icmpObject = new LinkedHashMap<>();
	try {
		
		 int transportLayerProtocol =0,c1=0;		             
            Map<String, Object> map2 = (LinkedHashMap<String, Object>) object;
			for (String key : map2.keySet()) {
				
				 if (key.equalsIgnoreCase("Hexdump")) {
					   c1 = 0 ;
				   }
				   if (key.equalsIgnoreCase("Error")) {
					   c1 = 0 ;
				   } 
				 
				   if (key.equalsIgnoreCase("TCP Source Port:")) {
					   	c1 = 1;
			    		transportLayerProtocol=1;
			    	}
				   if (key.equalsIgnoreCase("Request Method")){
						c1=2;
			    	}			    	
			    	if (key.equalsIgnoreCase("SCTP Source Port:")){
						c1=3;
						transportLayerProtocol=3;
					}
			    	if (key.equalsIgnoreCase("UDP Source Port:")) {
			    		c1 = 4;
			    		transportLayerProtocol=4;
			    	}
			    	if (key.equalsIgnoreCase("Type")) {
			    		c1 = 5;
			    		transportLayerProtocol=5;
					}
			    	if (key.equalsIgnoreCase("IP")) {
			    		c1 = 6;
					}
			    			
					if(c1==0)
						ipObject.put(key,map2.get(key));
					
					if(c1==1){
						tcpObject.put(key,map2.get(key));									
					}

					if(c1==2){
						httpObject.put(key,map2.get(key));
					}
					
					if(c1==3){
						sctpObject.put(key,map2.get(key));
					}
					if(c1==4) {
						udpObject.put(key,map2.get(key));
					}
					
					if(c1==5) {
						icmpObject.put(key,map2.get(key));
						
					}
					if(c1==6) {						
						List array = (List) map2.get("IP");
						
				//********ICMP NODE creation...
					if(array !=null) {
						
						int transportLayerProtocolIC =0,ic=0;
						
						 Map<Object, Object> ipObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> tcpObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> udpObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> httpObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> sctpObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> icmpObjectProtocol = new LinkedHashMap<>();
						 Map<Object, Object> icmpObjectIC = new LinkedHashMap<>();
						 
						for (Object objectIC : array) {
							
							li = new ArrayList<Object>();
														
							Map<String, Object> icmpLineMap = (Map<String, Object>) objectIC;
							
						for (String icmpLineKey : icmpLineMap.keySet()) {																						
							 if (icmpLineKey.equalsIgnoreCase("Hexdump")) {
								 ic = 0 ;
								 transportLayerProtocolIC=0;
							   }
							
							 if (icmpLineKey.equalsIgnoreCase("TCP Source Port:")) {
								 	ic = 1;
								   	transportLayerProtocolIC=1;
						    	}
							   if (icmpLineKey.equalsIgnoreCase("Request Method")){
								   ic=2;
						    	}			    	
						    	if (icmpLineKey.equalsIgnoreCase("SCTP Source Port:")){
						    		ic=3;
						    		transportLayerProtocolIC=3;
								}
						    	if (icmpLineKey.equalsIgnoreCase("UDP Source Port:")) {
						    		ic = 4;
						    		transportLayerProtocolIC=4;
						    	}
						    	if (icmpLineKey.equalsIgnoreCase("Type")) {
						    		ic = 5;
						    		transportLayerProtocolIC=5;
								}
						    								 
							 if(ic==0) {
								 ipObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								 icmpObjectIC.put("IP Packet", ipObjectIC);	
							 }
							 if(ic==1){
								 tcpObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));					
							 } 
								if(ic==2){
									httpObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								}
								
								if(ic==3){
									sctpObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								}
								if(ic==4) {
									udpObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								}
								if(ic==5) {
									icmpObjectProtocol.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								}							
						  }						
						if(transportLayerProtocolIC==1)
				  		{
							icmpObjectIC.put("TCP", tcpObjectIC);	
				  			if(ic==2) 
				  				tcpObjectIC.put("HTTP", httpObjectIC);
				  		}else if(transportLayerProtocolIC==3)
				  		{
							icmpObjectIC.put("SCTP", sctpObjectIC);	
				  			if(ic==2) 
				  				sctpObjectIC.put("HTTP", httpObjectIC);
				  		}
				  		
				  		else if(transportLayerProtocolIC==4)
				  		{
				  			icmpObjectIC.put("UDP", udpObjectIC);
				  			if(ic==2) 
				  				udpObjectIC.put("HTTP", httpObjectIC);

				  		}else if(transportLayerProtocolIC==5)
				  		{
				  			icmpObjectIC.put("ICMP", icmpObjectProtocol);	
				  		}
						li.add(icmpObjectIC);	
						}
				    }					
						
				 }
			}						
			
			
			if(transportLayerProtocol==1)
	  		{
	  			ipObject.put("TCP", tcpObject);	
	  			if(c1==2) 
	  				tcpObject.put("HTTP", httpObject);
	  		}
	  		if(transportLayerProtocol==3)
	  		{
	  			ipObject.put("SCTP", sctpObject);	
	  			if(c1==2) 
	  				sctpObject.put("HTTP", httpObject);
	  		}
	  		if(transportLayerProtocol==5)
	  		{
	  			ipObject.put("ICMP", icmpObject);	  			
	  			icmpObject.put("Other Protocol", li);	  			  			
	  		}
	  		else if(transportLayerProtocol==4)
	  		{
	  			ipObject.put("UDP", udpObject);
	  			if(c1==2) 
	  				udpObject.put("HTTP", httpObject);
	  		}
			
	}catch (Exception e) {
		//e.printStackTrace();
	}
	return ipObject;
	}
	

	public static String getFileString(MultipartFile file) throws CustomException {
		/**			  
		 *         Get multipart file data;			 		  
		 */	
		
		
		try {
			String fileString="";
	        byte[] bytes = file.getBytes();
	        Path path = Paths.get( file.getOriginalFilename());
	        Files.write(path, bytes);
	       // fileString = new String(bytes);	 
	        
	        Scanner scanner = new Scanner(new String(bytes));	  
	        scanner.useDelimiter("#"); 
	      
		  while(scanner.hasNext()) {
			  fileString = scanner.nextLine();
		        if(!scanner.nextLine().contains("#")) {  
		        	fileString = fileString + "\n"+scanner.next();
		        		        	
		        }	  
		  }
		  
		
	        // Close the scanner 
	        scanner.close(); 	
	        return fileString;
	        
		}catch (Exception e) {
			throw new CustomException("Exception occured while reading the file");
		}
		
		
		
		
	}
	
	
	/**
	 * 
	 * @author Priti Kumari
	 * 
	 *         Not in use	
	 *		   Used for decoding from base64 to string
	 */	
		//	IPPacketVerifier packetRule=null;
		//	public List<Object> getFileObj(IPPacket ipObj) throws CustomException, com.parser.customError.CustomException {
		//		byte[] valueDecoded = Base64.decodeBase64(ipObj.getFileString());
		//		String fileString = new String(valueDecoded);		
		//		List<Object> ipobj = null;		
		//		try {		
		//		   ipobj = packetRule.verifyIPPacket(fileString);	
		//		}catch (Exception e) {
		//		}
		//		return ipobj;
		//	}
	
	

}