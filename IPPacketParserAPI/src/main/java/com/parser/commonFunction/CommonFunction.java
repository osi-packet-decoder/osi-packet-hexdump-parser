package com.parser.commonFunction;

import java.util.List;

import com.parser.customError.CustomException;
import com.parser.model.IPPacket;

public interface CommonFunction {

	List<Object> getFileObj(IPPacket fileObj) throws CustomException, com.parser.customError.CustomException;
}
