package com.parser.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

//@JsonInclude(JsonInclude.Include.NON_ABSENT)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UDPPacket {

	private String sourcePort;
	private String destinationPort;
	private String totalLength;	
	private String checkSum;
	private String protocol;
	private String payload;
	private String error;
	
	private Map<String,String> httpObject;
	
	public Map<String, String> getHttpObject() {
		return httpObject;
	}
	public void setHttpObject(Map<String, String> httpObject) {
		this.httpObject = httpObject;
	}
	public String getSourcePort() {
		return sourcePort;
	}
	public void setSourcePort(String sourcePort) {
		this.sourcePort = sourcePort;
	}
	public String getDestinationPort() {
		return destinationPort;
	}
	public void setDestinationPort(String destinationPort) {
		this.destinationPort = destinationPort;
	}
	public String getTotalLength() {
		return totalLength;
	}
	public void setTotalLength(String totalLength) {
		this.totalLength = totalLength;
	}
	public String getCheckSum() {
		return checkSum;
	}
	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	
	
}
