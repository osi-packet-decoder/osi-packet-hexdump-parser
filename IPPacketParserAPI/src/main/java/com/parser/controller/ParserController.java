package com.parser.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parser.commonFunction.CommonFunctionImpl;
import com.parser.customError.CustomErrorType;
import com.parser.model.IPPacket;
import com.parser.service.IPPacketVerifier;


@RestController
@Component 

public class ParserController {
	//@Autowired CommonFunction commonfunc;
	//IPPacketVerifier packetRule=null;
	
	@SuppressWarnings({ "static-access" })
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/ipPacketParser", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> getTcpData(@RequestBody  IPPacket ipObj) {	
		
		try {	
			 List<Object> li=null;
			// Gson gson = new Gson();
			 Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String hexDump = ipObj.getHexDump();
			if (hexDump == null || hexDump == "" ) 
				return new ResponseEntity<Object>(new CustomErrorType("hexdump is missing."), HttpStatus.BAD_REQUEST);			
			else {					
								
				List<Object> ippacket= IPPacketVerifier.verifyIPPacket(hexDump);
				li = new ArrayList<Object>();
				for (int i = 0; i < ippacket.size(); i++) {
					
					@SuppressWarnings("unchecked")
					Map<Object, Object> ipObject = CommonFunctionImpl.getNodeObject(ippacket.get(i));
					 Map<Object, Object> rootObject = new LinkedHashMap<>();					 	
					 rootObject.put("IP Packet "+(i+1),ipObject);			
						if(ipObject.size()!=0 || ipObject !=null ) {
							String json = gson.toJson(rootObject, LinkedHashMap.class);
							li.add(json);
						}												
			     }					
				   return new ResponseEntity<>(li.toString(), HttpStatus.OK);
				}							
		} catch (Exception e) {
			return new ResponseEntity<>(new CustomErrorType(e.getMessage()), HttpStatus.BAD_REQUEST);
		}		
	}	
	
	@RequestMapping(value = "/uploadBulkIpPackets", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")	
	public @ResponseBody ResponseEntity<Object> fileUpload(@RequestParam("file") MultipartFile file ) throws IOException {
	try {
		 List<Object> li=null;
		if (file.isEmpty()) 
			return new ResponseEntity<Object>(new CustomErrorType("File is Empty"), HttpStatus.BAD_REQUEST);			   
		else { 
			li=new ArrayList<Object>();
			String fileString = CommonFunctionImpl.getFileString(file);	 			
			  
			 Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Gson gson = new Gson();			 
			 List<Object> ippacket= IPPacketVerifier.verifyIPPacket(fileString);
			 
			for (int i = 0; i < ippacket.size(); i++) {				
				 @SuppressWarnings("unchecked")
				Map<Object, Object> ipObject = CommonFunctionImpl.getNodeObject(ippacket.get(i));
				 Map<Object, Object> rootObject = new LinkedHashMap<>();
				 rootObject.put("IP Packet "+(i+1),ipObject);					
					if(ipObject.size()!=0 || ipObject !=null ) {
						String json = gson.toJson(rootObject, LinkedHashMap.class);
						li.add(json);
					}	
			}					
				return new ResponseEntity<>(li.toString(), HttpStatus.OK);
			}			
			
		}catch (Exception e) {
			return new ResponseEntity<>(new CustomErrorType(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
		
	}

	
		
	}
	
	
