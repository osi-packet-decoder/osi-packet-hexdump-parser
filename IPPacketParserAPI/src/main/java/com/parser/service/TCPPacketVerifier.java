package com.parser.service;

import java.util.LinkedHashMap;
import java.util.List;

import com.parser.customError.CustomException;

public class TCPPacketVerifier {
	/**
	 * Parses the hexdump of TCPPacket
	 * 
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 * @author AmantyaTech
	 */
	public static LinkedHashMap<String, Object> verifyTCPPacket(String hexdump, String fullString)
			throws CustomException {
		LinkedHashMap<String, Object> outputMap = new LinkedHashMap();

		try {
			String tcpPacket = isValidTCPPacket(hexdump);

			String sourcePort = isValidSourcePort(hexdump.substring(0, 4));
			outputMap.put("Source Port", sourcePort);

			String destinationPort = isValidDestinationPort(hexdump.substring(4, 8));
			outputMap.put("Destination Port", destinationPort);

			String sequenceNumber = isVaildSequenceNo(hexdump.substring(8, 16));
			outputMap.put("Sequence Number", sequenceNumber);

			String acknowlegementNumber = isValidAcknowledgementNo(hexdump.substring(16, 24));
			outputMap.put("Acknowledgement Number", acknowlegementNumber);

			String dataOffsetHex = hexdump.substring(24, 25);
			outputMap.put(" Data Offset", "Hexadecimal: 0x" + dataOffsetHex);

			int lengthInWords = Integer.parseInt(dataOffsetHex, 16); // header in words
			int lengthInBytes = 4 * lengthInWords;
			if (lengthInBytes < 20 || lengthInBytes > 60)
				throw new CustomException("Header length not valid.... " + dataOffsetHex);
			outputMap.put("Header Length", lengthInBytes + " Bytes");

			String combinedInfoHex = hexdump.substring(25, 28);
			int combinedInfoInteger = Integer.parseInt(combinedInfoHex, 16);
			String combinedInfoBinary = Integer.toBinaryString(combinedInfoInteger);
			while (combinedInfoBinary.length() < 12) {
				combinedInfoBinary = "0" + combinedInfoBinary;
			}

			String reservedBinary = combinedInfoBinary.substring(0, 6);
			outputMap.put("Reserved", "Binary: " + reservedBinary);

			String controlBitsBinary = combinedInfoBinary.substring(6, 12);
			outputMap.put("Control Bit Flags", "Binary: " + controlBitsBinary);

			String URGbinary = controlBitsBinary.substring(0, 1);
			boolean urgentPointerFieldSignificantFlag = "1".equals(URGbinary);
			outputMap.put("Urgent Pointer Field Significant Flag", urgentPointerFieldSignificantFlag);

			String ACKbinary = controlBitsBinary.substring(1, 2);
			boolean acknowledgementFieldSignificantFlag = "1".equals(ACKbinary);
			outputMap.put("Acknowledgement Field Significant Flag", acknowledgementFieldSignificantFlag);

			String PSHbinary = controlBitsBinary.substring(2, 3);
			boolean pushFunctionFlag = "1".equals(PSHbinary);
			outputMap.put("Push Function Flag", pushFunctionFlag);

			String RSTbinary = controlBitsBinary.substring(3, 4);
			boolean resetTheConnectionFlag = "1".equals(RSTbinary);
			outputMap.put("Reset The Connection Flag", resetTheConnectionFlag);

			String SYNbinary = controlBitsBinary.substring(4, 5);
			boolean synchronizeSequenceNumbersFlag = "1".equals(SYNbinary);
			outputMap.put("Synchronize Sequence Numbers Flag", synchronizeSequenceNumbersFlag);

			String FINbinary = controlBitsBinary.substring(5, 6);
			boolean noMoreDataFromSenderFlag = "1".equals(FINbinary);
			outputMap.put("No More Data From Sender Flag", noMoreDataFromSenderFlag);

			String windowSizeHex = hexdump.substring(28, 32);
			if (windowSizeHex.length() != 4)
				throw new CustomException("Window Size not valid...." + windowSizeHex);
			int windowSizeDec = Integer.parseInt(windowSizeHex, 16);
			outputMap.put("Window Size", "Hexadecimal: 0x" + windowSizeHex + " , Decimal: " + windowSizeDec);

			String checksumHex = hexdump.substring(32, 36);
			if (checksumHex.length() != 4)
				throw new CustomException("Checksum not valid...." + checksumHex);
			outputMap.put("Checksum", "Hexadecimal: 0x" + checksumHex);

			String urgentPointerHex = hexdump.substring(36, 40);
			if (urgentPointerHex.length() != 4)
				throw new CustomException("Urgent Pointer not valid...." + urgentPointerHex);
			int urgentPointerDec = Integer.parseInt(urgentPointerHex, 16);
			outputMap.put("Urgent Pointer", "Hexadecimal: 0x" + urgentPointerHex + " , Decimal: " + urgentPointerDec);

			String totalLength = fullString.substring(4, 8);

			String headerLength = fullString.substring(1, 2);
			Integer l = (4 * Integer.parseInt(headerLength, 16));
			String lengthinbytes = l.toString() + " Bytes";
			// TCP over HTTP
			if (((Integer.parseInt(totalLength, 16)) - (4 * Integer.parseInt(headerLength)) - lengthInBytes) == 450) {
				outputMap.put("Application Layer Protocol", "HTTP");
				if (hexdump.length() > 40) {
					LinkedHashMap<String, String> map3 = new LinkedHashMap();
					map3 = HTTPPacketVerifier.verifyHTTPPacket(hexdump.substring(40), lengthinbytes);
					outputMap.putAll(map3);
				}
			}
			// TCP over TLS
			if (destinationPort.equals("443") || sourcePort.equals("443")) {
				if (hexdump.length() > 40 && ((hexdump.substring(40, 42).equals("14"))
						|| (hexdump.substring(40, 42).equals("15")) || (hexdump.substring(40, 42).equals("16"))
						|| (hexdump.substring(40, 42).equals("17")) || (hexdump.substring(40, 42).equals("18")))) {
					outputMap.put("Application Layer Protocol", "SSL Layer");
					LinkedHashMap<String, Object> sslObject = new LinkedHashMap();
					sslObject = TLSPacketVerifier.verifyTLSPacket(hexdump.substring(40), fullString);
					outputMap.putAll(sslObject);
				}
				// TCP over SMTP
				if (hexdump.length() > 40 && Integer.parseInt(destinationPort) == 25
						|| hexdump.length() > 40 && Integer.parseInt(sourcePort) == 25) {
					outputMap.put("Application Layer Protocol", "SMTP");
					List smtpList = SMTPPacketVerifier.verifySMTPPacket(hexdump.substring(40));
					outputMap.put("SMTP", smtpList);
				}
				// TCP over Daimeter Protocol
				if ((Integer.parseInt(sourcePort) == 6003 && Integer.parseInt(destinationPort) == 51742)
						|| (Integer.parseInt(sourcePort) == 3868 || Integer.parseInt(destinationPort) == 3868))
					if ((Integer.parseInt(sourcePort) == 6003 && Integer.parseInt(destinationPort) == 51742)) {
						outputMap.put("Application Layer Protocol", "Daimeter Protocol");
						if (hexdump.length() > 64) {
							List<LinkedHashMap<String, Object>> daimeterlist = DiameterProtocol
									.verifyDiameterProtocol(hexdump.substring(64));
							outputMap.put("Diameter Protocol", daimeterlist);
						}

					}
				// //TCP over SIP
				if (Integer.parseInt(sourcePort) == 5060 || Integer.parseInt(destinationPort) == 5060) {
					outputMap.put("Application Layer Protocol", "SIP");
					if (hexdump.length() > 40) {
						LinkedHashMap<String, Object> map3 = new LinkedHashMap();
						map3 = SIPPacketVerifier.verifySIPPacket(hexdump.substring(64), fullString);
						outputMap.putAll(map3);
					}

				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			outputMap.put("Error ", e.getMessage());

		}
		//LinkedHashMap<String, Object> tcpMap = new LinkedHashMap();
		//tcpMap.put("TCP", outputMap);
		return outputMap;
	}

	// used to verify if Acknowledgement number is valid
	private static String isValidAcknowledgementNo(String acknowledgementNumber) throws CustomException {
		if (acknowledgementNumber.length() != 8)
			throw new CustomException("Acknowledgement No not valid...." + acknowledgementNumber);
		return "0x" + acknowledgementNumber;
	}

	// used to verify if Sequence number is valid
	private static String isVaildSequenceNo(String sequenceNumber) throws CustomException {
		if (sequenceNumber.length() != 8)
			throw new CustomException("Sequence No not valid...." + sequenceNumber);
		return "0x" + sequenceNumber;
	}

	// used to verify if Destination Port is valid
	private static String isValidDestinationPort(String destinationPort) throws CustomException {
		if (destinationPort.length() != 4)
			throw new CustomException("DestinationPort not valid...." + destinationPort);
		else {
			return Integer.parseInt(destinationPort, 16) + "";
		}
	}

	// used to verify if Source Port is valid
	private static String isValidSourcePort(String sourcePort) throws CustomException {
		if (sourcePort.length() != 4)
			throw new CustomException("SourcePort not valid...." + sourcePort);
		else {
			return Integer.parseInt(sourcePort, 16) + "";
		}
	}

	// used to verify TCPPacket is valid
	private static String isValidTCPPacket(String hexdump) throws CustomException {
		if (hexdump.length() < 40)
			throw new CustomException("Invalid TCP Packet " + hexdump.substring(0));

		else
			return hexdump;
	}

}
