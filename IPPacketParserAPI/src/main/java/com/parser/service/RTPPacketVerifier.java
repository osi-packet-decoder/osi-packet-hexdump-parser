package com.parser.service;

import java.util.LinkedHashMap;

import com.parser.customError.CustomException;
public class RTPPacketVerifier {
	
	private enum Type {
		
		Audio,
		Video;
	}
	
	private enum PayloadType {	
		PCMU (0, Type.Audio.name()+ "(ITU-T G.711 PCM μ-Law )"),
		Reserved_FS_1016_CELP (1, Type.Audio.name()+ " (reserved, previously FS-1016 CELP )"),
		Reserved_G721_or_G726_32 (2, Type.Audio.name()+ " (reserved, previously ITU-T G.721 ADPCM )"),
		GSM (3, Type.Audio.name()+ " (European GSM Full Rate )"),
		G723 (4, Type.Audio.name()+ " (ITU-T G.723.1)"),
		DIV4(5, Type.Audio.name()+ " (IMA_ADPCM)"),
		PCMA(8,Type.Audio.name()+ " (ITU-T G.711 PCM A-Law )"),
		LPC(7,Type.Audio.name()+ " (Experimental Linear Predictive Coding)"),
		DVI4(6,Type.Audio.name()+ " (IMA ADPCM audio 64 kbit/s)"),
		L16(10,Type.Audio.name()+ " (Linear PCM 16-bit Stereo audio 1411.2 kbit/s)"),
		L16_1(10,Type.Audio.name()+ " (Linear PCM 16-bit audio 705.6 kbit/s)"),
	    QCELP(12,Type.Audio.name()+ " (	QUALCOMM_CODE_EXCITED_LINEAR_Prediction)"),
		Comfort_Noise (13, Type.Audio.name() + " (CN)"),
		MPA(14,Type.Audio.name() + " (MPEG-1 or MPEG-2 audio only)"),
		G728(15,Type.Audio.name() + " (ITU-T G.728 audio 16 kbit/s)"),
		DVI4_1(16,Type.Audio.name() + " (IMA ADPCM audio 44.1 kbit/s)"),
		DVI4_2(17,Type.Audio.name() +" (IMA ADPCM audio 88.2 kbit/s)" ),
		G729(18,Type.Audio.name() +" (ITU-T G.729 and G.729a audio 8 kbit/s)"),
		CELB(25,Type.Video.name() + " (	Sun)"),
		JPEG(26,Type.Video.name() + " (JPEG	)"),
		nv(28,Type.Video.name() + " (Xerox PARC's)"),
		H261(31,Type.Video.name() + " (ITU-T H.261 )"),
		MPV(32,Type.Video.name() + " (MPEG-1 and MPEG-2)"),
		MP2T(33,Type.Video.name() + " (MPEG-2 transport stream)"),
		H263(34,Type.Video.name() + " (H.263 video, first version)"),	
		Dynamic (null, null);
		
		Integer code;
		String description;
		
		private PayloadType(Integer code, String description) {
			this.code = code;
			this.description = description;
		}
		
		public static PayloadType identify(Integer code) {
			for (PayloadType e : PayloadType.values()) {
				if (e.code == code) {
					return e;
				}
			}
			return PayloadType.Dynamic;
		}
		
	}

	public static LinkedHashMap<String, Object> parseRTPPacket(String hexdump) throws CustomException {

		LinkedHashMap<String, Object> outputMap = new LinkedHashMap();

		try {

			String combinedInfo1Hex = hexdump.substring(0, 1);

			// TODO : parse 'Version', 'P', 'X' from combinedInfo1Hex
			int combinedInfo1Int = Integer.parseInt(combinedInfo1Hex, 16);
			String combinedInfo1Bin = Integer.toBinaryString(combinedInfo1Int);

			String versionBin = combinedInfo1Bin.substring(0, 2);
			int versionDec = Integer.parseInt(versionBin, 2);
			outputMap.put("Version", "Binary: " + versionBin + " , Decimal: " + versionDec);

			String pBin = combinedInfo1Bin.substring(2, 3);
			boolean pFlag = "1".equals(pBin);
			outputMap.put("Padding (P)", "Binary: " + pBin + " , Boolean: " + pFlag);

			String xBin = combinedInfo1Bin.substring(3, 4);
			boolean xFlag = "1".equals(pBin);
			outputMap.put("Extension(X)", "Binary: " + xBin + " , Boolean: " + xFlag);

			// ----------------------------------------------------------------------------
			
			String ccHex = hexdump.substring(1, 2);
			
			int ccDec = Integer.parseInt(ccHex, 16);
			String ccBin = Integer.toBinaryString(ccDec);
			
			while (ccBin.length() < 4) {
				ccBin = "0" + ccBin;
			}
			
			outputMap.put("Contributing Source Identifiers Count (CC)", "Hexadecimal: 0x" + ccHex + " , Binary: " + ccBin + " , Decimal: " + ccDec);
			
			// ----------------------------------------------------------------------------

			String combinedInfo2Hex = hexdump.substring(2, 3);
			String combinedInfo3Hex = hexdump.substring(3, 4);

			// TODO : parse 'M', 'PT' from combinedInfo2Hex
			int combinedInfo2Int = Integer.parseInt(combinedInfo2Hex, 16);
			String combinedInfo2Bin = Integer.toBinaryString(combinedInfo2Int);
			
			while (combinedInfo2Bin.length() < 4) {
				combinedInfo2Bin = "0" + combinedInfo2Bin;
			}
			
			int combinedInfo3Int = Integer.parseInt(combinedInfo3Hex, 16);
			String combinedInfo3Bin = Integer.toBinaryString(combinedInfo3Int);
			
			while (combinedInfo3Bin.length() < 4) {
				combinedInfo3Bin = "0" + combinedInfo3Bin;
			}

			String mBin = combinedInfo2Bin.substring(0, 1);
			boolean mFlag = "1".equals(mBin);
			outputMap.put("Marker (M)", "Binary: " + mBin + " , Boolean: " + mFlag);

			String ptBin = combinedInfo2Bin.substring(1, 4) + combinedInfo3Bin;
			int ptDec = Integer.parseInt(ptBin, 2);
			PayloadType payloadType = PayloadType.identify(ptDec);
			outputMap.put("Payload Type (PT)", "Binary: " + ptBin + " , Decimal: " + ptDec + " , Name: " + payloadType.name() + " for " + payloadType.description);
			
			// ----------------------------------------------------------------------------

			String sequenceNumberHex = hexdump.substring(4, 8);
			int sequenceNumberDec = Integer.parseInt(sequenceNumberHex, 16);
			outputMap.put("Sequence Number", "Hexadecimal: 0x" + sequenceNumberHex + " , Decimal: " + sequenceNumberDec);

			String timestampHex = hexdump.substring(8, 16);
			int timestampDec = Integer.parseInt(timestampHex, 16);
			outputMap.put("Timestamp", "Hexadecimal: 0x" + timestampHex + " , Decimal: " + timestampDec);

			String ssrcIdentifierHex = hexdump.substring(16, 24);
			int ssrcIdentifierDec = Integer.parseInt(ssrcIdentifierHex, 16);
			outputMap.put("Syncronization Source Identifier (SSRC Identifier)", "Hexadecimal: 0x" + ssrcIdentifierHex + " , Decimal: " + ssrcIdentifierDec);
			
			String payload = hexdump.substring(24);
			outputMap.put("Payload", "Hexadecimal: 0x" + payload);

			// ----------------------------------------------------------------------------
			
		/*	int csrcIdentifierLength = 8 * ccDec;
			
			if (csrcIdentifierLength > 0) {
				String csrcIdentifierHex = hexdump.substring(24, 24 + csrcIdentifierLength);
				outputMap.put("CSRC Identifiers", "Hexadecimal: 0x" + csrcIdentifierHex);
			}
			
			// ----------------------------------------------------------------------------
			
			String profileSpecificExtensionHeaderIdHex = hexdump.substring(24 + csrcIdentifierLength, 28 + csrcIdentifierLength);
			int profileSpecificExtensionHeaderIdDec = Integer.parseInt(profileSpecificExtensionHeaderIdHex, 16);
			outputMap.put("Profile Specific Extension Header Id", "Hexadecimal: 0x" + profileSpecificExtensionHeaderIdHex + " , Decimal: " + profileSpecificExtensionHeaderIdDec);
			
			String extensionHeaderLengthHex = hexdump.substring(28 + csrcIdentifierLength, 32 + csrcIdentifierLength);
			int extensionHeaderLengthDec = Integer.parseInt(extensionHeaderLengthHex, 16);
			outputMap.put("Extension Header Length", "Hexadecimal: 0x" + extensionHeaderLengthHex + " , Decimal: " + extensionHeaderLengthDec);
			
			// ----------------------------------------------------------------------------
			
			String extensionHeaderHex = hexdump.substring(32 + csrcIdentifierLength);
			outputMap.put("Extension Header (Hexadecimal)", extensionHeaderHex);
			
			String extensionHeaderStr = new String(DatatypeConverter.parseHexBinary(extensionHeaderHex), "UTF-16");
			outputMap.put("Extension Header (String)", extensionHeaderStr);
*/
		} catch (Exception e) {

			e.printStackTrace();
			outputMap.put("Error", e.getMessage());

		}

		return outputMap;
	}

	public static void main(String[] args) throws CustomException {

		// String hexDump = "8008684532a1067c44c61afbd5d5d55555d5d555d5d5555555545454545555555455555555d5d5d5d5d4d5d5d5d5d5d555d5d55555d5d5d5d5d55555d5d55555555555d5555455545555d5d55455d5d5d5d555d5d5d5555555d5d555d55555d55555d5d5d5d5d5d5d5d5d5d5d5d5d55555d555d5d5555555555455d555555555d5d55555d5d5d555d55555d555d5d5d5d555d5d555555555d5555555d555555555d5d5d5d4d5d4d5d5d5d5d5";
		 String hexDump = "8008e6984ef79bcf06c156fd545555545455d555d5d5555555d555d5c64d7a6d696b69607357f0e1e99595e9e0fdd374676c68686c645bfcfaf4f7cfc6c7d6d755565f5f5b464446474744455b5b595f5250505754d5d7d7d6d1d1d3d5555455d55555d5d5d5d454d555d5d5d5d5d5555555d555d4d5d5d554d5d555d55555d5d4d55455d5d555d5d5555555d555d555d554d5d5d4d5d555d455d555d555d55554d5d4d5d5d5d5d45455d555";
		// String hexDump = "8008684732a107bc44c61afbd55555d5d5d5d5d5d5d5d4d555d5d5d5d5d5d5d5d5d555d5d5555555555454d5d4d7d6d1d6d4d4545751535050565755d4d7d0d0d3d0d0d6d4d554565650515754d5d5d5d5d4d5d5d55555555555d555555754d555d5d5d5d4d4d555d55454d45454d5555454d5d5d4d4555455d5d5d5d4d5d555d5555454d4d5d454555455d45454d555d4d7d4d55454555555d4d4d5d55454d5d4d75554545554d4d4d55455";
		
		
	//	String hexDump = "800de6bd4ef7b2ef06c156fd3e";
		
		// String hexDump = "8088e6be4ef7b88f06c156fdd4d4d4d4d4d5d5d4d5d5d5d5d5d5d5d5d5d555d5d55554d5d55455555457575454d5555454545754d55555d5d4d5d5d4d5d5d5d55454545656575757545455d4d7d4d6d6d6d1d0d1d7d7d7d4d7d455d5555455d5d55555d55554545756505056545455d4dfc6dc5dd3d8505853594d5a5c5b5a5f5d57d3d0d3dfd8d9c7c4dfdcdfd9d9dfd555d1d75651515d5056505c5d5d5d53535356575155d45555d5d4d7";
		
		LinkedHashMap<String, Object> output = parseRTPPacket(hexDump);
		
		for (String key : output.keySet()) {
			System.out.println(key + " -> " + output.get(key));
		}

	}

}