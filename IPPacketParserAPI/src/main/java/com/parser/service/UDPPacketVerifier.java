package com.parser.service;

import java.util.LinkedHashMap;
import java.util.List;

import com.parser.customError.CustomException;

public class UDPPacketVerifier {
	/**
	 * Parses the Hexdump of UDPPacket
	 * 
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 * @author AmantyaTech
	 */
	public static LinkedHashMap<String, Object> verifyUDPPacket(String hexdump, String fullString)
			throws CustomException {
		LinkedHashMap<String, Object> outputMap = new LinkedHashMap();
		try {
			String udpPacket = isValidUDPPacket(hexdump);
			String sourcePort = isValidSourcePort(hexdump.substring(0, 4));
			outputMap.put("Source Port", sourcePort);
			String destinationPort = isValidDestinationPort(hexdump.substring(4, 8));
			outputMap.put("Destination Port", destinationPort);
			String totalLength = isValidTotalLength(hexdump.substring(8, 12));
			outputMap.put("Packet Total Length ", totalLength + " Bytes");
			String checksum = isValidChecksum(hexdump.substring(12, 16));
			outputMap.put("Checksum", "0x" + checksum);
			//UDP over DTLS
			if ((Integer.parseInt(sourcePort) == 6000 && Integer.parseInt(destinationPort) == 7000)
					|| (Integer.parseInt(sourcePort) == 7000 && Integer.parseInt(destinationPort) == 6000)) {
				outputMap.put("Security layer Protocol", "DTLS");
				if (hexdump.length() > 16) {
					List<LinkedHashMap<String, Object>> dtlslist = DTLSPacketVerifier.parse(hexdump.substring(16));
					outputMap.put("DTLS", dtlslist);
				}
			}
			//UDP over SIP
			if (Integer.parseInt(sourcePort) == 5060 || Integer.parseInt(destinationPort) == 5060) {
				outputMap.put("Application Layer Protocol", "SIP");
				if (hexdump.length() > 16) {
					LinkedHashMap<String, Object> map3 = new LinkedHashMap();
					map3 = SIPPacketVerifier.verifySIPPacket(hexdump.substring(16), fullString);
					outputMap.putAll(map3);
				}
			}
            //UDP over RTP
			if ((Integer.parseInt(sourcePort) >= 1024 && Integer.parseInt(sourcePort) <= 65536)
					|| (Integer.parseInt(destinationPort) >= 1024 && Integer.parseInt(destinationPort) <= 65536)
					|| (Integer.parseInt(sourcePort) == 6040 && Integer.parseInt(destinationPort) == 6020)
					|| (Integer.parseInt(sourcePort) == 6020 && Integer.parseInt(destinationPort) == 6040)) {

				outputMap.put("Application layer Protocol", "RTP");
				if (hexdump.length() > 16) {
					LinkedHashMap<String, Object> map4 = new LinkedHashMap();
					map4 = RTPPacketVerifier.parseRTPPacket(hexdump.substring(16));
					outputMap.put("RTP", map4);
				}
			}

		} catch (Exception e) {
			outputMap.put("Error ", e.getMessage());

		}
		return outputMap;
	}

	private static String isValidChecksum(String checksum) throws CustomException {
		if (checksum.length() != 4)
			throw new CustomException("Checksum not valid...." + checksum);
		return checksum;
	}

	private static String isValidTotalLength(String totalLength) throws CustomException {
		if (totalLength.length() != 4)
			throw new CustomException("Total Length not valid...." + totalLength);
		return Integer.parseInt(totalLength, 16) + "";
	}

	private static String isValidDestinationPort(String destinationPort) throws CustomException {
		if (destinationPort.length() != 4)
			throw new CustomException("DestinationPort not valid...." + destinationPort);
		return Integer.parseInt(destinationPort, 16) + "";
	}

	private static String isValidSourcePort(String sourcePort) throws CustomException {
		if (sourcePort.length() != 4)
			throw new CustomException("SourcePort not valid...." + sourcePort);
		return Integer.parseInt(sourcePort, 16) + "";
	}

	private static String isValidUDPPacket(String hexdump) throws CustomException {
		if (hexdump.length() > 0 && hexdump.length() < 16) {
			throw new CustomException("Invalid UDPacket " + hexdump.substring(0));
		} else
			return hexdump;
	}
}