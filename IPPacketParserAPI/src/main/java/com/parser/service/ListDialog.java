package com.parser.service;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;

import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parser.customError.CustomException;

class Parser 
extends JFrame 
implements ActionListener { 

// Components of the Form 
private Container c; 
private JLabel title,statusLabel; 

private JTextArea tadd; 
private JTree jt;

int clicked = 0;

private JButton sub,clear; 
private JButton reset, uplaod; 
JButton download = new JButton("Download Result"); 
private JLabel res,d ; 
private JTextArea resadd;
int c1;
JScrollPane scroll;
private JPanel pinkPanel,statusPanel,mainPanel,buttonPanel;
JFileChooser f1 = new JFileChooser(); 
Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
int screenHeight = screenSize.height;
int screenWidth = screenSize.width;
java.util.List<Object> map;
JSONArray json= null;
JPanel container = new JPanel();
String fileName1 = "com/parser/icon.png";
private BufferedImage myPicture;
String exc = null;
JScrollPane pane;



@SuppressWarnings({ "null", "unchecked" })
public Parser() 
{ 

    setTitle("IP Packet Decoder"); 
    setBounds(200, 0, 1000,screenHeight);
    setBackground(new Color(208, 224, 222));
    setDefaultCloseOperation(EXIT_ON_CLOSE); 
    setResizable(true); 
   setIconImage(createImage(fileName1).getImage());
    c = getContentPane();
    c.setLayout(new BorderLayout());

    c.setBackground(new Color(208, 224, 222));
    
    mainPanel = new JPanel();
    mainPanel.setBackground(new Color(208, 224, 222));

    mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER,screenWidth/2,screenHeight/114));

    pinkPanel = new JPanel();
pinkPanel.setBackground(Color.BLACK);

try {
	myPicture = ImageIO.read(getClass().getResource("/com/parser/verizon.jpg"));
} catch (IOException e2) {
	
}
JPanel panel = new JPanel();

panel.setLayout(new GridBagLayout());
GridBagConstraints gc = new GridBagConstraints();

JLabel channelNumber = new JLabel(new ImageIcon(myPicture));
gc.gridx = 0;
gc.gridy = 0;
gc.weightx = 1;
gc.anchor = GridBagConstraints.NORTHWEST;
//1st cordinate in inset pixel from vertical, 2nd cordinate pixel from left(x-axis),3rd coordinate from downward vertical
gc.insets = new Insets(3, 30, 3, 2);
title = new JLabel("IP Packet Decoder",SwingConstants.CENTER); 
title.setHorizontalAlignment(SwingConstants.CENTER);
title.setFont(new Font("Arial", Font.PLAIN, 30)); 
title.setPreferredSize(new Dimension(screenWidth,50));
title.setForeground(Color.WHITE);

panel.add(title);   
panel.add(channelNumber, gc);
panel.setBackground(Color.BLACK);
c.add(panel,BorderLayout.NORTH);


    
    container.setBackground(Color.WHITE);
    pane = new JScrollPane(container, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	pane.setPreferredSize(new Dimension(screenWidth/2, screenHeight/2));	
    tadd = new JTextArea("",6,60);
       tadd.setLineWrap(true);     
    JScrollPane js = new JScrollPane(tadd,
           JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
           JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    mainPanel.add(js);
    
    ImageIcon icon = new ImageIcon("com/parser/verizon.png");
    JLabel label = new JLabel(icon, JLabel.CENTER);
    add(label);
    
    JEditorPane jep = new JEditorPane("text/html","*Multiple hexdumps can be uploaded in a single text file seperated by new line. <a href='txt'>Click here</a> to download sample file.");
    jep.setEditable(false);
    jep.setBackground(new Color(208, 224, 222));
    jep.setForeground(Color.gray);

    jep.addHyperlinkListener(new HyperlinkListener() {
      public void hyperlinkUpdate(HyperlinkEvent hle) {
        if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType())) {
          if (hle.getDescription().equalsIgnoreCase("txt")) {
        	  String content;
            File file = null;
            String resource = "/com/parser/sample.txt";
            java.net.URL res = getClass().getResource(resource);
            if (res.getProtocol().equals("jar")) {
                try {
                    InputStream input = (InputStream) getClass().getResourceAsStream(resource);
                    file = File.createTempFile("tempfile", ".tmp");
                    OutputStream out = new FileOutputStream(file);
                    int read;
                    byte[] bytes = new byte[1024];
 
                    while ((read = input.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    out.close();
                    file.deleteOnExit();
                } catch (IOException ex) {
                    //ex.printStackTrace();
                }
            } else {
                //this will probably work in your IDE, but not from a JAR
                file = new File(res.getFile());
            }
 		try {
 			content = new String(Files.readAllBytes(file.toPath()));
     	    String home = System.getProperty("user.home");
 			@SuppressWarnings("resource")
 			FileWriter filew = new FileWriter(home+"/Downloads/sample.txt");
           filew.write(content);
             filew.flush();
 
 		} catch (IOException e1) {
 			
 		}
        }
        }
      }
    }); 
    jep.setOpaque(true);
    mainPanel.add(jep);
          
    buttonPanel = new JPanel();
    buttonPanel.setBackground(new Color(208, 224, 222));  

   
    sub = new JButton("Decode"); 
    sub.setFont(new Font("Arial", Font.PLAIN, 15)); 
    sub.setForeground(Color.WHITE);
    sub.setBackground(Color.BLACK);
    sub.addActionListener(this); 
    sub.addActionListener(ev -> {
    	try {
			map = new ArrayList<Object>();

			String str = tadd.getText();
			map = IPPacketVerifier.verifyIPPacket(str);
		} catch ( CustomException e1) {
			//e1.printStackTrace();
		}
    });

    sub.setPreferredSize(new Dimension(130,30));
    sub.setOpaque(true);
    sub.setBorderPainted(false);
    buttonPanel.add(sub);
    

    FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
	f1.setFileFilter(filter);
    uplaod = new JButton("Upload"); 
    uplaod.setFont(new Font("Arial", Font.PLAIN, 15)); 
    uplaod.addActionListener(this); 

    uplaod.addActionListener(ev -> {
		String strCurrentLine = null;
		try {

        int returnVal = f1.showOpenDialog(c);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File file = f1.getSelectedFile();
          
            if(file.getName().endsWith(".txt")) {
            BufferedReader input = new BufferedReader(new InputStreamReader(
                new FileInputStream(file)));
           
			while ((strCurrentLine = input.readLine()) != null) {
				 if (strCurrentLine.startsWith("<<") && (strCurrentLine.endsWith(">>"))) {
					 strCurrentLine= strCurrentLine.replace("<<", "");
					 strCurrentLine= strCurrentLine.replace(">>", "");
						//list.add(strCurrentLine);
			            java.util.List<String> list = Arrays.asList(strCurrentLine);

						  for (int i = 0; i < list.size(); i++) {
						tadd.append(""+list.get(i));
						tadd.append("\n");
						map = new ArrayList<Object>();
						try {
							String str = tadd.getText();
							map = IPPacketVerifier.verifyIPPacket(str);
						} catch ( CustomException e1) {
							//e1.printStackTrace();
						}
	                }

				 }
				 
			
			
			}
			tadd.setText(null);
			 JOptionPane.showMessageDialog(null, 
	                  "Your file is uploaded successfully!", 
	                  "Success Message", 
	                  JOptionPane.PLAIN_MESSAGE);
        }
            else {
            	 JOptionPane.showMessageDialog(null, 
                         "Your file is not supported.", 
                         "Failure Message", 
                         JOptionPane.ERROR_MESSAGE);
            }
        }
        
        }catch (Exception e) {
        	 
          }
         
        
      });
    uplaod.setForeground(Color.WHITE);
    uplaod.setBackground(Color.BLACK);
    uplaod.setPreferredSize(new Dimension(130,30));
    uplaod.setOpaque(true);
    uplaod.setBorderPainted(false);
    buttonPanel.add(uplaod); 
    mainPanel.add(buttonPanel);
    
    clear = new JButton("Clear"); 
    clear.setFont(new Font("Arial", Font.PLAIN, 15)); 
    clear.setForeground(Color.WHITE);
    clear.setBackground(Color.BLACK);
    clear.addActionListener(this); 
    clear.setPreferredSize(new Dimension(130,30));
    clear.setOpaque(true);
    clear.setBorderPainted(false);
    buttonPanel.add(clear);
    
    download.setFont(new Font("Arial", Font.PLAIN, 15)); 
    download.addActionListener(ev -> {
        try {
        	  List<Object> li=null;
        	  //Used Gson Pretty Printing for arranging fields in download file
   			Gson gson = new GsonBuilder().setPrettyPrinting().create();
   			 String currentKey = null;
                Object str =null;
                String st =null;
                String string =null;
            	  String rootKey = null;
  				li = new ArrayList<Object>();
              for (int i = 0; i < map.size(); i++) {

              	 Map rootObject = new LinkedHashMap<>();
  				 Map ipObject = new LinkedHashMap<>();
  				 Map tcpObject = new LinkedHashMap<>();
  				 Map udpObject = new LinkedHashMap<>();
  				 Map httpObject = new LinkedHashMap<>();
  				 Map sctpObject = new LinkedHashMap<>();
  				 Map icmpObject = new LinkedHashMap<>();
  				 Map otherIPObject = new LinkedHashMap<>();
  				 Map otherTransportObject = new LinkedHashMap<>();
  				 Map otherIPICMPObject = new LinkedHashMap<>();
  				 Map otherTransportICMPObject = new LinkedHashMap<>();

              int transportLayerProtocol =0;
              Map<String, Object> map2 = (Map<String, Object>) map.get(i);
  			for (String key : map2.keySet()) {
  			   if (key.equalsIgnoreCase("Hexdump")) {
  				   c1 = 0 ;
  			   }
  			   if (key.equalsIgnoreCase("Error")) {
  				   c1 = 9 ;
  			   } 
  			   if (key.equalsIgnoreCase("Source Port")) {
  		    		c1 = 1;
  		    		transportLayerProtocol=1;
  		    	}
  		    	if (key.equalsIgnoreCase("UDP Source Port:")) {
  		    		c1 = 4;
  		    		transportLayerProtocol=4;
  		    	}
  		    	if (key.equalsIgnoreCase("SCTP Source Port:")) {
  		    		c1 = 3;
  		    		transportLayerProtocol=3;

  		    	}

  		    	if (key.equalsIgnoreCase("Request Method"))
  		    	{
  					c1=2;
  		    	}
  		    	if (key.equalsIgnoreCase("Type")) {
					c1=5;
		    		transportLayerProtocol=5;
  		    	}
		    	if (key.equalsIgnoreCase("IP")) {
					c1=6;
		    	}
  		    	
  				if(c1==0) {
  					ipObject.put(key,map2.get(key));
  					if(ipObject.containsKey("version"))
  						st = (String) ipObject.get("version");
  				}
  				if(c1==1) {
  					tcpObject.put(key,map2.get(key));
  					if(ipObject.containsKey("Transport Layer Protocol"))
  						currentKey = (String) ipObject.get("Transport Layer Protocol");

  				}
  				if(c1==5) {
  					icmpObject.put(key,map2.get(key));
  					if(ipObject.containsKey("Transport Layer Protocol"))
  						currentKey = (String) ipObject.get("Transport Layer Protocol");

  				}

  				if(c1==3) {
  					sctpObject.put(key,map2.get(key));
  					if(ipObject.containsKey("Transport Layer Protocol"))
  						currentKey = (String) ipObject.get("Transport Layer Protocol");
  				}
  				if(c1==4) {
  					udpObject.put(key,map2.get(key));
  					if(ipObject.containsKey("Transport Layer Protocol"))
  						currentKey = (String) ipObject.get("Transport Layer Protocol");
  				}

  				if(c1==2)
  				{
  					httpObject.put(key,map2.get(key));
  				}
  				if(c1==6) {
	  				List array = (List) map2.get(key);
					for(int l= 0; l < array.size();l++) {

					 StringBuilder b = new StringBuilder();
					 array.forEach(b::append);
					 String bst = b.toString();
					 List<String> list3 = Arrays.asList((bst.split(",")));
					 if(list3.get(1).contains("4")) {
                   	 List<String> trans = Arrays.asList(list3.get(14).split("=")[1]);
                        String transportnode = trans.toString();
                        transportnode = transportnode.substring(1, transportnode.length()-1);           

                    for (int k = 0; k < 15; k++) {

                    	otherIPObject.put(list3.get(k).split("=")[0], list3.get(k).split("=")[1]);
                    	otherIPICMPObject.put("IP", otherIPObject);
                    }

                    for (int k = 15; k < list3.size(); k++) {

						 otherTransportObject.put(list3.get(k).split("=")[0], list3.get(k).split("=")[1]);
                    	otherTransportICMPObject.put(transportnode, otherTransportObject);
				                     }

                    }
                    if(list3.get(1).contains("6")) {
                   	 List<String> trans = Arrays.asList(list3.get(9).split("=")[1]);
                        String transportnode = trans.toString();
                        transportnode = transportnode.substring(1, transportnode.length()-1);           

                        for (int k = 0; k < 10; k++) {
                        
                        	otherIPObject.put("IP", list3.get(k));

                        }

	                     for (int k = 10; k < list3.size(); k++) {
	                    	 List<String> list4 = Arrays.asList((list3.get(k).split(",")));
	                    	 otherTransportObject.put(transportnode, list4.get(0));
   									
   								
   				                     }
                    }
                        }
	  			}
  				if(c1==9) {
  					ipObject.put(key,map2.get(key));
  	    			string = ipObject.toString();
  				}

  				
  		    }

  			
      		if(transportLayerProtocol==1)
      		{
	  			ipObject.put("TCP", tcpObject);	
	  			if(tcpObject.containsKey("Application Layer Protocol"))
					str = (String) tcpObject.get("Application Layer Protocol");
	  			if(c1==2)
		  			tcpObject.put("HTTP", httpObject);	


      		}
      		if(transportLayerProtocol==3)
      		{
	  			ipObject.put("SCTP", sctpObject);
	  			if(sctpObject.containsKey("Application Layer Protocol"))
					str = (String) sctpObject.get("Application Layer Protocol");
	  			if(c1==2)
		  			sctpObject.put("HTTP", httpObject);	

      		}
      		if(transportLayerProtocol==5)
      		{
	  			ipObject.put("ICMP", icmpObject);	
  				if(c1==6) {
  		  			icmpObject.put("Other Protocol", otherIPICMPObject+""+otherTransportICMPObject);	
  				}
  				if(c1==2)
		  			icmpObject.put("HTTP", httpObject);	

      		}

      		else if(transportLayerProtocol==4)
      		{
      			ipObject.put("UDP",udpObject);
      			if(udpObject.containsKey("Application Layer Protocol"))
  					str = (String) udpObject.get("Application Layer Protocol");
      			
      		}
              
      		rootObject.put("IP Packet "+(i+1),ipObject);
  				String json = gson.toJson(rootObject, LinkedHashMap.class);
  				li.add(json);
  				
              }
  			if(li.size()>1) {
  				 rootKey = "IP"+"-HexDecode";
  			}
  			else{
                 if(string!=null && string.contains("Error")) {
  				 rootKey = "Error"+"-HexDecode";
                  }
                 else {
              	   if(currentKey==null) {
             			   rootKey = "IPv"+st; 
                  	   }
                  if(currentKey!=null) {
                    	 
              	   if(str==null) {
         			   rootKey = "IPv"+st+"-"+currentKey; 
              	   }
              	   if(str!=null) {
                	   rootKey = "IPv"+st+"-"+currentKey+"-"+str; 
              	   }
              	   }
                 }
  			}

      	    String home = System.getProperty("user.home");
      	    Date date = new Date();
      	    Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
      		@SuppressWarnings("resource")
  			FileWriter filew = new FileWriter(home+"/Downloads/"+rootKey+"("+formatter.format(date)+")"+".json");
              filew.write(li.toString());
                filew.flush();

              
              
            } catch (Exception e1) {
              e1.printStackTrace();
            }
          
        }); 
  download.setPreferredSize(new Dimension(180,30));
  download.setForeground(Color.WHITE);
  download.setBackground(Color.BLACK);
  download.setOpaque(true);
  download.setVisible(true);
  download.setBorderPainted(false);


    c.add(mainPanel, BorderLayout.CENTER); 

    setVisible(true); 
    
    statusPanel = new JPanel();

    statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
    statusPanel.setSize(this.getWidth(), 50);
    statusLabel = new JLabel("Copyright © 2020. All Rights Reserved.", SwingConstants.CENTER);
    statusLabel.setLocation(0, screenHeight-20);
    statusLabel.setHorizontalAlignment(SwingConstants.CENTER);
    statusLabel.setForeground(Color.WHITE);
    statusLabel.setFont(new Font("Arial", Font.PLAIN, 12)); 
    statusLabel.setPreferredSize(new Dimension(screenWidth,18));
    statusLabel.setVisible(true);
    statusPanel.setBackground(Color.BLACK);
    statusPanel.add(statusLabel);
    c.add(statusPanel, BorderLayout.SOUTH);
    
} 

  

public ImageIcon createImage(String path) {
	return new ImageIcon(getClass().getClassLoader().getResource(path));
   }

// method actionPerformed() 
// to get the action performed 
// by the user and act accordingly 
public void actionPerformed(ActionEvent e) {
	if (e.getSource() == sub || e.getSource() == uplaod) {


//		map = new ArrayList<Object>();
//		String exc = null;
//		try {
//			String str = tadd.getText();
//			map = IPPacketVerifier.verifyIPPacket(str);
//		} catch ( com.test.service.CustomException e1) {
//			//e1.printStackTrace();
//		}
			//tadd.setText(null);
			String command = e.getActionCommand();
			DefaultMutableTreeNode root1 = null;
			DefaultMutableTreeNode ip = null;
		    DefaultMutableTreeNode tcp = null;
			DefaultMutableTreeNode udp = null;
			DefaultMutableTreeNode sctp = null;
			DefaultMutableTreeNode http123 =null;
			DefaultMutableTreeNode sip =null;
			DefaultMutableTreeNode message =null;
			DefaultMutableTreeNode header =null;
			DefaultMutableTreeNode smtp =null;
			DefaultMutableTreeNode tls =null;
			DefaultMutableTreeNode dtls =null;
			DefaultMutableTreeNode option =null;
			DefaultMutableTreeNode diameter =null;
			DefaultMutableTreeNode icmp =null;
			DefaultMutableTreeNode ftp =null;
			DefaultMutableTreeNode icmp_ip =null;
			DefaultMutableTreeNode rtp = null;


			 root1 = new DefaultMutableTreeNode("Decoded Packet");
			
			for (int i = 0; i < map.size(); i++) {
				 ip = new DefaultMutableTreeNode("IP Packet "+(i+1));
			     tcp = new DefaultMutableTreeNode("Transmission Control Protocol(TCP)");
				 udp = new DefaultMutableTreeNode("User Datagram Protocol(UDP)");
				sctp = new DefaultMutableTreeNode("Stream Control Transmission Protocol(SCTP)");
				option = new DefaultMutableTreeNode("Optional Parameters");
				 http123 = new DefaultMutableTreeNode("HyperText Transfer Protocol(HTTP)");
				 sip = new DefaultMutableTreeNode("Session Initiation Protocol(SIP)");
				 message = new DefaultMutableTreeNode("Message Body");
				 header = new DefaultMutableTreeNode("Message Header");
				 smtp = new DefaultMutableTreeNode("Simple Mail Transfer Protocol(SMTP)");
				 tls = new DefaultMutableTreeNode("Transport Layer Security(TLS)");
				 dtls = new DefaultMutableTreeNode("Datagram Transport Layer Security(DTLS)");
				 diameter = new DefaultMutableTreeNode("Diameter Protocol");
			     icmp = new DefaultMutableTreeNode("Internet Control Message Protocol(ICMP)");
			     ftp = new DefaultMutableTreeNode("File Transfer Protocol(FTP)");
			     icmp_ip = new DefaultMutableTreeNode("Internet Protocol(IP)");
				rtp = new DefaultMutableTreeNode("Real-time Transport Protocol(RTP)");


				
				
			Map<String, Object> map2 = (Map<String, Object>) map.get(i);
			for (String key : map2.keySet()) {
			   if (key.equalsIgnoreCase("Hexdump")) {
				   c1 = 0 ;
			   }
			   if (key.equalsIgnoreCase("Error")) {
				   c1 = 0 ;
			   }
			   if (key.equalsIgnoreCase("Source Port")) {
		    		c1 = 1;
		    	}
		    	if (key.equalsIgnoreCase("UDP Source Port:")) {
		    		c1 = 4;
		    	}
		    	if (key.equalsIgnoreCase("SCTP Source Port:"))
		    		c1 = 3;
		    	
		    	if (key.equalsIgnoreCase("Request Method"))
					c1=2;
		    	if (key.equalsIgnoreCase("SIP")) {
					c1=5;
		    	}
		    	if (key.equalsIgnoreCase("Optional Parameters")) {
					c1=6;
		    	}
		    	if (key.equalsIgnoreCase("SMTP"))
					c1=7;
		    	if (key.equalsIgnoreCase("TLS"))
					c1=8;
		    	if (key.equalsIgnoreCase("DTLS"))
					c1=9;
		    	if (key.equalsIgnoreCase("Diameter Protocol"))
					c1=10;
		    	if (key.equalsIgnoreCase("Type"))
					c1=11;
		    	if (key.equalsIgnoreCase("IP"))
					c1=13;
		    	
		    	if (key.equalsIgnoreCase("FTP"))
					c1=12;
		    	if (key.equalsIgnoreCase("RTP"))
					c1 = 14;
		    	
				root1.add(ip);  					

				//Add fields to IP Packet node
				if(c1==0) {
					ip.add(new DefaultMutableTreeNode(key + ":"+map2.get(key)));
				}
				
				//Add fields to TCP Packet node
				if(c1==1) {
					ip.add(tcp);
					tcp.add(new DefaultMutableTreeNode(key + ":"+map2.get(key)));
				}

				//Add fields to SMTP Packet node
				if (c1 == 7) {
						tcp.add(smtp);
						List array = (List) map2.get(key);
						for (Object object : array) {
							Map<String, Object> smtpLineMap = (Map<String, Object>) object;
							DefaultMutableTreeNode node = new DefaultMutableTreeNode(smtpLineMap.get("Command Line"));
							smtpLineMap.remove("Command Line");
							if (smtpLineMap.keySet().size() > 0) {
								for (String smtpLineKey : smtpLineMap.keySet()) {
									DefaultMutableTreeNode child = new DefaultMutableTreeNode(smtpLineKey + ":" + smtpLineMap.get(smtpLineKey));
									node.add(child);
								}
							}
							smtp.add(node);
						
					}
				}
				
				//Add fields to TCP Packet node
				if (c1 == 8) {
						tcp.add(tls);
						List array = (List) map2.get(key);
						for (Object object : array) {
							Map<String, Object> smtpLineMap = (Map<String, Object>) object;
							DefaultMutableTreeNode node = new DefaultMutableTreeNode("TLS Record Layer");

							if (smtpLineMap.keySet().size() > 1) {
								for (String smtpLineKey : smtpLineMap.keySet()) {
									DefaultMutableTreeNode child = new DefaultMutableTreeNode(smtpLineKey + ":" + smtpLineMap.get(smtpLineKey));
									node.add(child);
									if(smtpLineKey.equalsIgnoreCase("Handshake Protocol")) {
										Map<String, Object> smtpLineMap1 = (Map<String, Object>)  smtpLineMap.get(smtpLineKey);
	         							if (smtpLineMap1.keySet().size() > 1) {
	         								for (String smtpLineKey1 : smtpLineMap1.keySet()) {
	        									DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(smtpLineKey1 + ":" + smtpLineMap1.get(smtpLineKey1));
	        									child.add(child1);
	        									if(smtpLineKey1.equalsIgnoreCase("Extension")) {
	        										List value = (List) smtpLineMap1.get(smtpLineKey1);
	        										for (Object object1 : value) {
	        											Map<String, Object> smtpLineMap2 = (Map<String, Object>) object1;
	        											String nodeName = "Extension: "+smtpLineMap2.get("Type");
	        											DefaultMutableTreeNode node1 = new DefaultMutableTreeNode(nodeName);
                                                          child1.add(node1);
	        											if (smtpLineMap2.keySet().size() > 1) {
	        												for (String smtpLineKey2 : smtpLineMap2.keySet()) {
	        													DefaultMutableTreeNode child2 = new DefaultMutableTreeNode(smtpLineKey2 + ":" + smtpLineMap2.get(smtpLineKey2));
	        													node1.add(child2);
	        												}
	        											}
	        										}
	        									}
	        									
	         								}
	         							}
	         							
									}
								}
							}
							tls.add(node);
						}
					
				}
				if (c1 == 9) {
					if (key == "DTLS") {
						udp.add(dtls);
						// smtp.add(new DefaultMutableTreeNode("smtp:" + map2.get(key)));
						List array = (List) map2.get(key);
						for (Object object : array) {
							Map<String, Object> smtpLineMap = (Map<String, Object>) object;
							DefaultMutableTreeNode node = new DefaultMutableTreeNode("DTLS Record Layer");
							DefaultMutableTreeNode child = null;
							if (smtpLineMap.keySet().size() > 1) {
								for (String smtpLineKey : smtpLineMap.keySet()) {
									child = new DefaultMutableTreeNode(smtpLineKey + ":" + smtpLineMap.get(smtpLineKey));
									node.add(child);
									if(smtpLineKey.equalsIgnoreCase("Handshake Protocol")) {
										Map<String, Object> smtpLineMap1 = (Map<String, Object>)  smtpLineMap.get(smtpLineKey);
	         							if (smtpLineMap1.keySet().size() > 1) {
	         								for (String smtpLineKey1 : smtpLineMap1.keySet()) {
	        									DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(smtpLineKey1 + ":" + smtpLineMap1.get(smtpLineKey1));
	        									child.add(child1);
	        									if(smtpLineKey1.equalsIgnoreCase("Extension")) {
	        										List value = (List) smtpLineMap1.get(smtpLineKey1);
	        										for (Object object1 : value) {
	        											Map<String, Object> smtpLineMap2 = (Map<String, Object>) object1;
	        											String nodeName = "Extension: "+smtpLineMap2.get("Type");
	        											DefaultMutableTreeNode node1 = new DefaultMutableTreeNode(nodeName);
                                                          child1.add(node1);
	        											if (smtpLineMap2.keySet().size() > 1) {
	        												for (String smtpLineKey2 : smtpLineMap2.keySet()) {
	        													DefaultMutableTreeNode child2 = new DefaultMutableTreeNode(smtpLineKey2 + ":" + smtpLineMap2.get(smtpLineKey2));
	        													node1.add(child2);
	        												}
	        											}
	        										}
	        									}
	        									
	         								}
	         							}
	         							
									}
								}
							}
							dtls.add(node);
						}
					}
				}
				if(c1==2)
				{
					tcp.add(http123);
					http123.add(new DefaultMutableTreeNode(key + ":" +map2.get(key)));
				}
				if(c1==10)
				{
					if (key == "Diameter Protocol") {

					tcp.add(diameter);
					List array = (List) map2.get(key);
					for (Object object : array) {
						Map<String, Object> smtpLineMap = (Map<String, Object>) object;
						String treeNode = "Diameter Layer: "+smtpLineMap.get("Diameter Version");;
						DefaultMutableTreeNode node = new DefaultMutableTreeNode(treeNode);
						DefaultMutableTreeNode child = null;
						if (smtpLineMap.keySet().size() > 1) {
							for (String smtpLineKey : smtpLineMap.keySet()) {
								child = new DefaultMutableTreeNode(smtpLineKey + ":" + smtpLineMap.get(smtpLineKey));
								node.add(child);
							
								if(smtpLineKey.equalsIgnoreCase("AVPs")) {
									List value = (List) smtpLineMap.get(smtpLineKey);
									for (Object object1 : value) {
										Map<String, Object> smtpLineMap1 = (Map<String, Object>) object1;
										String nodeName = "AVPs: "+smtpLineMap1.get("AVP code");;
										DefaultMutableTreeNode node1 = new DefaultMutableTreeNode(nodeName);
                                          child.add(node1);
                                          if (smtpLineMap1.keySet().size() > 1) {
												for (String smtpLineKey1 : smtpLineMap1.keySet()) {
													DefaultMutableTreeNode child2 = new DefaultMutableTreeNode(smtpLineKey1 + ":" + smtpLineMap1.get(smtpLineKey1));
													node1.add(child2);
													if(smtpLineKey1.equalsIgnoreCase("AVP flags (Parsed)")) {
														Map<String, Object> smtpLineMap2 = (Map<String, Object>)  smtpLineMap1.get(smtpLineKey1);
					         							if (smtpLineMap2.keySet().size() > 0) {
					         								for (String smtpLineKey2 : smtpLineMap2.keySet()) {
					        									DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(smtpLineKey2 + ":" + smtpLineMap2.get(smtpLineKey2));
					        									child2.add(child1);
												}
					         							}
													}
												
												}
												}
								}
							}
						}
						}
					diameter.add(node);
					}
				}
				}
					
				if(c1==5)
				{
					if (map2.containsValue("TCP")) {
					tcp.add(sip);
					}
					
					if (map2.containsValue("UDP")) {
						udp.add(sip);
						}
					
						List array = (List) map2.get(key);
						for (Object object : array) {
							Map<String, Object> smtpLineMap = (Map<String, Object>) object;
							if (smtpLineMap.keySet().size() > 0) {
								for (String smtpLineKey : smtpLineMap.keySet()) {
									DefaultMutableTreeNode child = new DefaultMutableTreeNode(smtpLineKey + ":" + smtpLineMap.get(smtpLineKey));
									sip.add(child);
									if(smtpLineKey.equalsIgnoreCase("Message Header")) {
										String value = (String) smtpLineMap.get(smtpLineKey);
										value = value.substring(1, value.length()-1);           
										String[] keyValuePairs = value.split(",");              
										Map<String,String> map = new HashMap<>();               

										for(String pair : keyValuePairs)                        
										{
											if(pair.contains(": ")) {
       									DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(pair);
                                               child.add(child1);
										}
										}
									}
									if(smtpLineKey.equalsIgnoreCase("Message Body")) {
										DefaultMutableTreeNode node = new DefaultMutableTreeNode("Session Description Protocol(SDP)");
										String value = (String) smtpLineMap.get(smtpLineKey);
										value = value.substring(1, value.length()-1);           
										String[] keyValuePairs = value.split(",");              

										for(String pair : keyValuePairs)                        
										{
											if(pair.contains("=")) {
												pair = pair.substring(2, pair.length()-1);           

       									DefaultMutableTreeNode child2 = new DefaultMutableTreeNode(pair);
                                               child.add(node);
       										node.add(child2);

										}
										}

	         							
									}
								}
							}
						}
					}
					
				
						
							
			

				if(c1==3) {
					
					ip.add(sctp);
					sctp.add(new DefaultMutableTreeNode(key + ":" +map2.get(key)));
				}
				if(c1==6) {
					sctp.add(option);
					String val =( (String) map2.get(key));
                     List<String> list1 = Arrays.asList(val.split("}"));
                     for (int j = 0; j < list1.size()-1; j++) {
						
						String value = list1.get(j).substring(3, list1.get(j).length());           
 						list1.removeAll(Arrays.asList("", null));
 						DefaultMutableTreeNode node1 = null;
 						String nodeName = null;
 						if(list1.get(j).split("=")[0].contains("Parameter Type")) {
 	 						List<String> list3 = Arrays.asList(list1.get(j).split("=")[1].split(","));
							nodeName = "Parameter: "+list3.get(0);
						
							node1 = new DefaultMutableTreeNode(nodeName);
							option.add(node1);
						}
 						List<String> list2 = Arrays.asList(value.split(","));

 						for (int k = 0; k < list2.size(); k++) {
										if(list2.get(k).contains("=")) {
		
      					                 node1.add(new DefaultMutableTreeNode(list2.get(k)));
									}
								
								}
 						}
 						
                     }
				
				if(c1==4) {
					ip.add(udp);
					udp.add(new DefaultMutableTreeNode(key + ":"+map2.get(key)));
				}
				if(c1==11)
				{
					ip.add(icmp);
					icmp.add(new DefaultMutableTreeNode(key + ":" +map2.get(key)));

				}
				if(c1==13)
				{
					DefaultMutableTreeNode other = new DefaultMutableTreeNode("Other Protocols");

					icmp.add(other);
					List array = (List) map2.get(key);
					for(int l= 0; l<array.size();l++) {
					 StringBuilder b = new StringBuilder();
					 array.forEach(b::append);
					 String st = b.toString();
					 List<String> list3 = Arrays.asList((st.split(",")));
                     other.add(icmp_ip);
                     if(list3.get(1).contains("4")) {
                    	 List<String> trans = Arrays.asList(list3.get(14).split("=")[1]);
                         String transportnode = trans.toString();
                         transportnode = transportnode.substring(1, transportnode.length()-1);           

                     for (int k = 0; k < 15; k++) {
								DefaultMutableTreeNode child = new DefaultMutableTreeNode(list3.get(k));
								icmp_ip.add(child);
                     }
						DefaultMutableTreeNode node = new DefaultMutableTreeNode(transportnode);

                     for (int k = 15; k < list3.size(); k++) {

								icmp_ip.add(node);

									 List<String> list4 = Arrays.asList((list3.get(k).split(",")));
									DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(list4.get(0));
                                  node.add(child1);
								
				                     }

                     }
                     if(list3.get(1).contains("6")) {
                    	 List<String> trans = Arrays.asList(list3.get(9).split("=")[1]);
                         String transportnode = trans.toString();
                         transportnode = transportnode.substring(1, transportnode.length()-1);           

                         for (int k = 0; k < 10; k++) {
    								DefaultMutableTreeNode child = new DefaultMutableTreeNode(list3.get(k));
    								icmp_ip.add(child);
                         }
							DefaultMutableTreeNode node = new DefaultMutableTreeNode(transportnode);

	                     for (int k = 10; k < list3.size(); k++) {

    								icmp_ip.add(node);

    									 List<String> list4 = Arrays.asList((list3.get(k).split(",")));
    									DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(list4.get(0));
                                      node.add(child1);
    								
    				                     }

                         }
					}
					   //System.out.println(b);
				}
				if(c1==12)
				{
					tcp.add(ftp);
					List array = (List) map2.get(key);
					for (Object object : array) {
						Map<String, Object> smtpLineMap = (Map<String, Object>) object;
						DefaultMutableTreeNode node = new DefaultMutableTreeNode(smtpLineMap.get("Line"));
						if (smtpLineMap.keySet().size() > 0) {
							for (String smtpLineKey : smtpLineMap.keySet()) {
								DefaultMutableTreeNode child = new DefaultMutableTreeNode(smtpLineKey + ":" + smtpLineMap.get(smtpLineKey));
								node.add(child);
							}
						}
						ftp.add(node);
					}
				}
				if (c1 == 14) {
					if (map2.containsValue("UDP")) {
						LinkedHashMap<String, Object> rtpParsedMap = (LinkedHashMap<String, Object>) map2.get(key);
						for (String rtpAttribute : rtpParsedMap.keySet()) {
							rtp.add(new DefaultMutableTreeNode(
									rtpAttribute + ":" + rtpParsedMap.get(rtpAttribute)));
						}
						udp.add(rtp);
					}
				}
		    }
			}
		    clicked= clicked+1;	
		    DefaultMutableTreeNode err = new DefaultMutableTreeNode("Invalid Packet");
		    err.add(new DefaultMutableTreeNode("Error : "+exc));
			if(clicked<=1 ) {
				if (exc==null)
				{
					jt = new JTree(root1);	
				}
				else
				{
					jt = new JTree(err);
				}
				
				jt.setVisible(true);
				Icon icon = new ImageIcon(getClass().getClassLoader().getResource("com/parser/ima.png"));
				DefaultTreeCellRenderer render = (DefaultTreeCellRenderer) jt.getCellRenderer();
		        render.setLeafIcon(icon);
				 jt.expandRow(1);
				    jt.scrollRowToVisible(4);	
				container.add(jt);
		        mainPanel.add(pane);
		        mainPanel.add(download);

				this.revalidate();
				this.repaint();
			}else {
					DefaultTreeModel model = (DefaultTreeModel) jt.getModel();
			        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
			        root.removeAllChildren();
			        mainPanel.remove(download);
			        mainPanel.remove(pane);
					container.remove(jt);
			        model.reload();	
			        model.setRoot(null);		        
			        if (exc==null)
					{
						jt = new JTree(root1);
					}
					else
					{
						jt = new JTree(err);
					}
					jt.setVisible(true);
					Icon icon = new ImageIcon(getClass().getClassLoader().getResource("com/parser/ima.png"));
					DefaultTreeCellRenderer render = (DefaultTreeCellRenderer) jt.getCellRenderer();
			        render.setLeafIcon(icon);
					 jt.expandRow(1);
					    jt.scrollRowToVisible(4);	
					container.add(jt);
					mainPanel.add(pane);
					mainPanel.add(download);

					this.revalidate();
					this.repaint();
					clicked=1;
			}
			        
    } 

    else if (e.getSource() == clear) { 
        String def = "";         
        tadd.setText(def);

    } 
	}

	}

	//Driver Code 
	class FirstPage{ 
	
	public static void main(String[] args) throws Exception 
	{ 
		Parser f = new Parser(); 
		
	} 
	} 
