package com.parser.service;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import com.parser.customError.CustomException;


public class HTTPPacketVerifier {	
	public static LinkedHashMap<String,String> verifyHTTPPacket(String hexdump,String fullHexdump) throws CustomException
	{	
		
		LinkedHashMap<String,String> outputMap= new LinkedHashMap();			
		try {			
			//String reqMethod = isValidRequestMethod(fullHexdump.substring(80,200));			
			String reqMethod = isValidRequestMethod(hexdump.substring(0,120));			
			 //outputMap.put("Request Method:" ,reqMethod );			 
			if(reqMethod.equalsIgnoreCase("GET ")) {													
				//String requestMethodStr = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(fullHexdump.substring(104)), "UTF-8");
				String requestMethodStr = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(hexdump.substring(24)), "UTF-8");
				List<String> list = Arrays.asList(requestMethodStr.split("[\\r\\n]+"));					
				for (int i = 0; i < list.size()-1; i++) {	
					outputMap.put("Request Method", list.get(0).split("\\s+")[0]);	
					outputMap.put("Request URI", list.get(0).split("\\s+")[1]);
					outputMap.put("Request Version", list.get(0).split("\\s+")[2]);
					outputMap.put(list.get(i+1).split(":")[0], list.get(i+1).split(":")[1]);			        	
				}			
		}
		else {			
			String requestMethodStr = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(hexdump.substring(0)), "UTF-8");
			//String requestMethodStr = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(fullHexdump.substring(80)), "UTF-8");
			List<String> list = Arrays.asList(requestMethodStr.split("[\\r\\n]+"));	
			for (int i = 0; i < list.size()-1; i++) {
				outputMap.put("Request Method", list.get(0).split("\\s+")[0]);	
				outputMap.put("Request URI", list.get(0).split("\\s+")[1]);
				outputMap.put("Request Version", list.get(0).split("\\s+")[2]);									
				if(list.get(i+1).split(":")[0].equalsIgnoreCase("Content-Length"))
					outputMap.put("Payload", list.get(i+1).split(":")[1]+" bytes");	
			   if(list.get(i+1).split(":")[0].equalsIgnoreCase("INVITE sip")) break;					
			   outputMap.put(list.get(i+1).split(":")[0], list.get(i+1).split(":")[1]);					
			}	
		 }
	
			
		} catch (Exception e) {
			outputMap.put("Error", e.getMessage());
		}			
			return outputMap;		
	}
	
		
	private static String isValidConnection(String hexConnection ) throws CustomException, UnsupportedEncodingException {
		if(hexConnection .length()!=28) {
			throw new CustomException("Connection is not valid ...."+hexConnection);
		}else {
			 byte[] bytes10 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexConnection);
				return new String(bytes10, "UTF-8");
		}		
	}


	private static String isValidCookiePair(String hexCookiePair) throws CustomException, UnsupportedEncodingException {
		if( hexCookiePair.length()!=38) {
			throw new CustomException("Cookie Pair is not valid ...."+hexCookiePair);
		}else {
			 byte[] bytes9 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexCookiePair);
				return new String(bytes9, "UTF-8");
		}		
	}

	private static String isValidCookie(String hexCookie) throws CustomException, UnsupportedEncodingException {
		if( hexCookie.length()!=290) {
			throw new CustomException("Cookie is not valid ...."+hexCookie);
		}else {
			 byte[] bytes8 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexCookie);
				return new String(bytes8, "UTF-8");
		}
		
	}

	private static String isValidAcceptEncoding(String hexAcceptEncoding) throws CustomException, UnsupportedEncodingException {
		if( hexAcceptEncoding.length()!=32) {
			throw new CustomException("Accept Encoding is not valid ...."+ hexAcceptEncoding);
		}else {
			 byte[] bytes7 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexAcceptEncoding);
				return new String(bytes7, "UTF-8");
		}
	}
	private static String isValidAcceptLanguage(String  hexAcceptLanguage )throws CustomException, UnsupportedEncodingException  {

		if( hexAcceptLanguage.length()!=34) {
			throw new CustomException("Accept Language is not valid ...."+ hexAcceptLanguage);
		}else {
			 byte[] bytes6 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexAcceptLanguage);
				return new String(bytes6, "UTF-8");
		}
		
	}


	private static String isValidAccept(String hexUserAccept) throws UnsupportedEncodingException, CustomException {
		if(hexUserAccept.length() != 132) {			
			throw new CustomException("User Accept length is not valid ...."+ hexUserAccept);
		}else {
			 byte[] bytes5 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexUserAccept);
			return new String(bytes5, "UTF-8");
		}	
	}

	private static String isValidUserAgent(String hexUserAgent) throws CustomException, UnsupportedEncodingException {
		if(hexUserAgent.length() != 138) {			
			throw new CustomException("User Agent length is not valid ...."+ hexUserAgent);
		}else {
			 byte[] bytes4 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexUserAgent);
			return new String(bytes4, "UTF-8");
		}	
	}

	private static String isValidHost(String hexHost)throws CustomException, UnsupportedEncodingException {
		if(hexHost.length() != 26) {			
			throw new CustomException("Http Request host length is not valid ...."+ hexHost);
		}else {
			byte[] bytes3 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexHost);
			return new String(bytes3, "UTF-8");
		}	
	}

	private static String isValidRequestVersion(String hexRequestVersion)throws CustomException, UnsupportedEncodingException {
		
		if(hexRequestVersion.length() != 18) {			
			throw new CustomException("Http Request version length is not valid ...."+ hexRequestVersion);
		}else {
			 byte[] bytes2 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexRequestVersion);
			return new String(bytes2, "UTF-8");
		}	
	}

	private static String isValidRequestUri(String hexUri)throws CustomException, UnsupportedEncodingException {
		if(hexUri.length() != 2) {			
			throw new CustomException("Http Request URI length is not valid ...."+ hexUri);
		}else {
			byte[] bytes1 = javax.xml.bind.DatatypeConverter.parseHexBinary(hexUri);
			return new String(bytes1, "UTF-8");
		}		
	}

	private static String isValidRequestMethod(String hexRequest) throws CustomException, UnsupportedEncodingException {
		String postReq=hexRequest.substring(0, 8);
		byte[] bytes = javax.xml.bind.DatatypeConverter.parseHexBinary(postReq);
		String postStr= new String(bytes);
		
		if(postStr.equalsIgnoreCase("POST")) {
			return postStr;
		}
		String gettReq=hexRequest.substring(24, 32);
		byte[] bytesGet = javax.xml.bind.DatatypeConverter.parseHexBinary(gettReq);
		String getStr= new String(bytesGet);
		if(getStr.equalsIgnoreCase("GET ")) {
			return getStr;
		}
		else 
			throw new CustomException("Http Request method unknown"+ hexRequest);
		}
		
	}	
			


