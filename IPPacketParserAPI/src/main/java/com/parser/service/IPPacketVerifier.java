package com.parser.service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import com.parser.customError.CustomException;

public class IPPacketVerifier {
	/**
	 * Parses the hexdump of IPPacket
	 * @request string
	 * @response List<Object>
	 * @author AmantyaTech
	 */
	public static List<Object> verifyIPPacket(String hexdumpString) throws CustomException {
		String hexdump = null;
		List<Object> ipList = null;
		String regx = "[\\|\t\n\r]+";
		List<String> list = Arrays.asList(hexdumpString.split(regx));
		// ******** Remove empty index from list
		list = list.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
		ipList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			hexdump = list.get(i).replaceAll("\\s", "");
			LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();
			String hexStart = "<<", hexEnd = ">>";
			if ((hexdump.matches("[0-9A-Fa-f<>]+"))) {
				try {
					if (((hexdump.startsWith(hexStart)) && (hexdump.endsWith(hexEnd)))) {
						hexdump = hexdump.replace(hexEnd, "");
						hexdump = hexdump.replace(hexStart, "");
					} else {
						hexdump = hexdump;
					}

					boolean isV4 = false, isV6 = false;
					String IPVer = null;
					if (hexdump.length() > 0) {
						IPVer = hexdump.substring(0, 1);
						if (IPVer.equals("4")) {
							isV4 = true;
						} else if (IPVer.equals("6")) {
							isV6 = true;
						}
					}

					outputMap = new LinkedHashMap<>();
					String ipPacket = isValidIPPacket(hexdump, IPVer);
					outputMap.put("Hexdump", hexdump);
					String version = isValidVersion(hexdump.substring(0, 1));
					outputMap.put("version", version);
                  //IPv4 Header ..
					if (isV4) {
						String headerLength = isValidHeaderLength(hexdump.substring(1, 2));
						outputMap.put("Header Length", headerLength + " Bytes");
						String sourcetype = isValidPrecedence(hexdump.substring(2, 4));
						while (sourcetype.length() < 8)
							sourcetype = "0" + sourcetype;

						Integer pre = Integer.parseInt(sourcetype.substring(0, 3), 2);
						outputMap.put("Precedence", pre.toString());

						if (sourcetype.charAt(3) == '1')
							outputMap.put("Precedence : Minimize Delay Requested", "true");
						if (sourcetype.charAt(4) == '1')
							outputMap.put("Precedence : Maximize Throughput Requested", "true");
						if (sourcetype.charAt(5) == '1')
							outputMap.put("Precedence : Maximize Reliability Requested", "true");
						if (sourcetype.charAt(6) == '1')
							outputMap.put("Precedence :Minimize Cost Requested", "true");

						String totalLength = Integer.parseInt(version) == 4
								? isValidTotalLength(hexdump.substring(4, 8))
								: isValidTotalLength(hexdump.substring(8, 12));
						outputMap.put("Total Length", totalLength + " Bytes");

						String id = isValidIdentification(hexdump.substring(8, 12));
						outputMap.put("Identification", id);

						String fragment = isValidfragmentmentLength(hexdump.substring(12, 16));
						String fragmentBinary = Integer.toBinaryString(Integer.parseInt(hexdump.substring(12, 16), 16));
						while (fragmentBinary.length() < 16)
							fragmentBinary = "0" + fragmentBinary;
						String fragmentFlags = fragmentBinary.substring(0, 3);

						if (fragment.charAt(1) == '1')
							outputMap.put("Do not fragment Packet", "true");
						else
							outputMap.put("Can be fragmented", "true");
						if (fragment.charAt(2) == '1')
							outputMap.put("More fragments pending", "true");
						else
							outputMap.put("No more fragments pending", "true");

						if (fragment.charAt(1) != '1' && fragment.charAt(2) != '1')
							outputMap.put("fragmentation Offset ",
									(Integer.parseInt(hexdump.substring(12, 16), 16)) + "");
						else
							outputMap.put("fragmentation Offset", "0");

						String TimeToLive = isValidTimeToLive(hexdump.substring(16, 18));
						outputMap.put("Time to live: ", TimeToLive);

						String checksum1 = isValidChecksumLength(hexdump.substring(20, 24));
						outputMap.put("Header Checksum: ", "0x" + checksum1);

						String ipPayload = ((Integer.parseInt(totalLength)) - (Integer.parseInt(headerLength)))
								+ " Bytes";
						outputMap.put("IP Payload", ipPayload);

						String sourceIp = isValidIPv4Address(hexdump.substring(24, 32));
						outputMap.put("Source IP Address", sourceIp);

						String destinationIp = isValidIPv4Address(hexdump.substring(32, 40));
						outputMap.put("Destination IP", destinationIp);

						String protocol = isValidProtocolLength(hexdump.substring(18, 20));
						if (Integer.parseInt(protocol) == 1) {
							outputMap.put("Transport Layer Protocol", "ICMP");

							// IP to ICMP
							if (hexdump.length() > 40) {
								LinkedHashMap<String, Object> map3 = new LinkedHashMap();
							    map3 = ICMPPacketVerifier.verifyICMPPacket(hexdump.substring(40),hexdump);
								outputMap.putAll(map3);
							}
						}

						if (Integer.parseInt(protocol) == 2)
							outputMap.put("Transport Layer Protocol", "IGMP");

						if (Integer.parseInt(protocol) == 89)
							outputMap.put("Transport Layer Protocol", "OSPF");

						if (Integer.parseInt(protocol) == 17) {
							outputMap.put("Transport Layer Protocol", "UDP");
							if (hexdump.length() > 40) {
								LinkedHashMap<String, Object> map3 = new LinkedHashMap();
								map3 = UDPPacketVerifier.verifyUDPPacket(hexdump.substring(40), hexdump);
								outputMap.putAll(map3);
							}
						}
						if (Integer.parseInt(protocol) == 6) {
							outputMap.put("Transport Layer Protocol", "TCP");
							if (hexdump.length() > 40) {
								LinkedHashMap<String, Object> map2 = new LinkedHashMap();
								map2 = TCPPacketVerifier.verifyTCPPacket(hexdump.substring(40), hexdump);
								outputMap.putAll(map2);
							}
						}

						if (Integer.parseInt(protocol) == 132) {
							outputMap.put("Transport Layer Protocol", "SCTP");
							if (hexdump.length() > 40) {
								LinkedHashMap<String, Object> map3 = new LinkedHashMap();
								map3 = SCTPPacketVerifier.verifySCTPPacket(hexdump.substring(40), hexdump);
								outputMap.putAll(map3);
							}
						}
					}
					//IPV6 Header...
					if (isV6) {

						String trafficClass = isValidTrafficClass(hexdump.substring(1, 3));
						outputMap.put("Traffic Class", "0x" + trafficClass);

						String flowLabel = isValidFlowLabel(hexdump.substring(3, 8));
						outputMap.put("Flow Label", "0x" + flowLabel);

						String payloadLength = isValidPayloadLength(hexdump.substring(8, 12));
						outputMap.put("Payload Length", payloadLength + " ");

						String hopLimit = isValidHopLimit(hexdump.substring(14, 16));
						outputMap.put("HopLimit", hopLimit + " ");

						String sourceIp = isValidSourceIPv6Address(hexdump.substring(16, 48));
						outputMap.put("Source IP Address", sourceIp);

						String destinationIp = isValidSourceIPv6Address(hexdump.substring(48, 80));
						outputMap.put("Destination IP", destinationIp);

						String nextHeader = isValidNextHeader(hexdump.substring(12, 14));
						outputMap.put("Next Header", nextHeader + " ");
						if (Integer.parseInt(nextHeader) == 1)
							outputMap.put("Transport Layer Protocol", "ICMP");

						if (Integer.parseInt(nextHeader) == 2)
							outputMap.put("Transport Layer Protocol", "IGMP");

						if (Integer.parseInt(nextHeader) == 89)
							outputMap.put("Transport Layer Protocol", "OSPF");

						if (Integer.parseInt(nextHeader) == 17) {
							outputMap.put("Transport Layer Protocol", "UDP");
							if (hexdump.length() > 80) {
								LinkedHashMap<String, Object> map3 = new LinkedHashMap();
								map3 = UDPPacketVerifier.verifyUDPPacket(hexdump.substring(80), hexdump);
								outputMap.putAll(map3);
							}
						}
						if (Integer.parseInt(nextHeader) == 6) {
							outputMap.put("Transport Layer Protocol", "TCP");
							if (hexdump.length() > 80) {
								LinkedHashMap<String, Object> map2 = new LinkedHashMap();
								map2 = TCPPacketVerifier.verifyTCPPacket(hexdump.substring(80), hexdump);
								outputMap.putAll(map2);
							}
						}
						if (Integer.parseInt(nextHeader) == 132) {
							outputMap.put("Transport Layer Protocol", "SCTP");
							if (hexdump.length() > 80) {
								LinkedHashMap<String, Object> map3 = new LinkedHashMap();
								map3 = SCTPPacketVerifier.verifySCTPPacket(hexdump.substring(80), hexdump);
								outputMap.putAll(map3);
							}
						}
					}

				} catch (Exception e) {
					outputMap.put("Error", e.getMessage());
				}
				ipList.add(outputMap);
			} else {
				throw new CustomException("Hexdump format is not valid " + hexdump);
			}
		}
		return ipList;
	}

	private static String isValidHopLimit(String hexHopLimit) throws CustomException {
		if (hexHopLimit.length() != 2) {
			throw new CustomException("Hop Limit is not valid " + hexHopLimit);
		} else
			return Integer.parseInt(hexHopLimit, 16) + "";
	}

	private static String isValidNextHeader(String hexNextHeader) throws CustomException {
		if (hexNextHeader.length() != 2) {
			throw new CustomException("Next header length is not valid " + hexNextHeader);
		} else
			return Integer.parseInt(hexNextHeader, 16) + "";
	}

	private static String isValidPayloadLength(String hexPayloadLength) {
		return Integer.parseInt(hexPayloadLength, 16) + "";
	}

	private static String isValidFlowLabel(String hexFlowLabel) throws CustomException {
		if (hexFlowLabel.length() != 5) {
			throw new CustomException(" Flow label is not valid " + hexFlowLabel);
		} else
			return (hexFlowLabel);
	}

	private static String isValidTrafficClass(String hexTrafficClass) {
		return (hexTrafficClass);
	}

	private static String isValidProtocolLength(String hexProtocol) throws CustomException {

		if (hexProtocol.length() != 2) {
			throw new CustomException("Protocol length is not valid " + hexProtocol);
		} else
			return Integer.parseInt(hexProtocol, 16) + "";
	}

	private static String isValidChecksumLength(String hexCheckSum) throws CustomException {
		if (hexCheckSum.length() != 4)
			throw new CustomException("CheckSum length is not valid " + hexCheckSum);
		else {
			return Integer.parseInt(hexCheckSum, 16) + "";
		}
	}

	private static String isValidTimeToLive(String hexTimeToLive) throws CustomException {
		if (hexTimeToLive.length() != 2)
			throw new CustomException("TimeToLive length is not valid " + hexTimeToLive);
		else
			return Integer.parseInt(hexTimeToLive, 16) + " Hops";
	}

	private static String isValidfragmentmentLength(String hexfragmentment) throws CustomException {
		if (hexfragmentment.length() != 4)
			throw new CustomException("fragmentmentation length is not valid " + hexfragmentment);
		else {
			String fragmentBinary = Integer.toBinaryString(Integer.parseInt(hexfragmentment, 16));
			while (fragmentBinary.length() < 16)
				fragmentBinary = "0" + fragmentBinary;
			String fragmentFlags = fragmentBinary.substring(0, 3);
			return fragmentFlags;
		}
	}

	private static String isValidIdentification(String hexidentification) throws CustomException {
		if (hexidentification.length() != 4)
			throw new CustomException("Identification length is not valid " + hexidentification);
		else
			return " " + Integer.parseInt(hexidentification, 16);
	}

	private static String isValidTotalLength(String totalLength) throws CustomException {
		if (totalLength.length() != 4)
			throw new CustomException("Total Length is not valid " + totalLength + " valid length is 20 bytes");
		else
			return Integer.parseInt(totalLength, 16) + "";
	}

	private static String isValidPrecedence(String precedence) throws CustomException {
		if (precedence.length() > 2)
			throw new CustomException("Invalid precedence Length " + precedence);
		else {
			String s_t_bin = Integer.toBinaryString(Integer.parseInt(precedence, 16));
			return s_t_bin;
		}
	}

	private static String isValidIPv4Address(String ipAddress) throws CustomException {
		if (ipAddress.length() != 8)
			throw new CustomException("IP Address length invalid " + ipAddress + " it should not be greater than 8");
		else {
			String ipBinary = Long.toBinaryString(Long.parseLong(ipAddress, 16));
			while (ipBinary.length() < 32)
				ipBinary = "0" + ipBinary;
			String sip = Integer.parseInt(ipBinary.substring(0, 8), 2) + "."
					+ Integer.parseInt(ipBinary.substring(8, 16), 2) + "."
					+ Integer.parseInt(ipBinary.substring(16, 24), 2) + "."
					+ Integer.parseInt(ipBinary.substring(24, 32), 2);
			return sip;
		}
	}

	private static String isValidSourceIPv6Address(String ipAddress) throws CustomException {

		if (ipAddress.length() != 32)
			throw new CustomException("IP Address length invalid " + ipAddress + " it should not be greater than 8");
		else {
			String output = ipAddress.substring(0, 4) + ":" + ipAddress.substring(4, 8) + ":"
					+ ipAddress.substring(8, 12) + ":" + ipAddress.substring(12, 16) + ":" + ipAddress.substring(16, 20)
					+ ":" + ipAddress.substring(20, 24) + ":" + ipAddress.substring(24, 28) + ":"
					+ ipAddress.substring(28, 32);
			return (output);
		}
	}

	private static String isValidHeaderLength(String headerLength) throws CustomException {
		Integer l = (4 * Integer.parseInt(headerLength, 16));
		if (l < 20 || l > 60)
			throw new CustomException("Header Length not valid " + (4 * Integer.parseInt(headerLength))
					+ " range should be 20 -60 bytes");
		else
			return l.toString();
	}

	private static String isValidVersion(String hexVersion) throws CustomException {
		int versionInInteger = Integer.parseInt(hexVersion, 16);
		if (versionInInteger != 4 && versionInInteger != 6)
			throw new CustomException(" IP version is invalid " + hexVersion + " expected IP Version is 4 or 6");
		else
			return new String(versionInInteger + "");
	}

	private static String isValidIPPacket(String hexdump, String iPVer) throws CustomException {
		if (hexdump.length() < 40) {
			throw new CustomException("IPPacket should not be less than 40 digits " + hexdump);
		}
		if (iPVer.equals("4") && hexdump.length() < 40)
			throw new CustomException("IPPacket should not be less than 40 digits " + hexdump);
		if (iPVer.equals("6") && hexdump.length() < 80)
			throw new CustomException("IPPacket should not be less than 80 digits " + hexdump);
		else
			return hexdump;
	}

}