package com.parser.service;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.parser.customError.CustomException;

public class SMTPPacketVerifier {
	
	private enum Command {
		EHLO,
		HELO,
		MAIL,
		RCPT,
		DATA,
		RSET,
		VRFY,
		EXPN,
		HELP,
		NOOP,
		QUIT;
	}
	private static Set<String> commands = new HashSet<String>();
	
	static {
		for (Command command : Command.values()) {
			commands.add(command.name());
		}
	}

	private static String checkForCommand(String line, LinkedHashMap<String, String> outputMap) {
		String[] splitBySpace = line.split(" ");
		String parsedString = new String();
				for (String term : splitBySpace) {
			if (commands.contains(term)) {
				outputMap.put("Command", term);
			} else {
				parsedString += " " + term;
			}
		}
		return parsedString.trim();
	}	
	public static LinkedHashMap<String, String> parseSMTPPacket(String line) {
		LinkedHashMap<String, String> outputMap = new LinkedHashMap();
		outputMap.put("Line", line);
		line = checkForCommand(line,outputMap);
		String[] parts = (line.split(":"));
		if (parts.length == 2)
			outputMap.put(parts[0], parts[1]);

		else if (parts.length == 1) {
			LinkedList<String> numbers = new LinkedList<String>();
			Pattern p = Pattern.compile("\\d+");
			Matcher m = p.matcher(line);
			while (m.find()) {
				numbers.add(m.group());
			}
			if (numbers.size() > 0) {
				outputMap.put("Code", numbers.get(0));
			}
			String remaining = line;

			for (String number : numbers) {
				remaining = remaining.replace(number, "");
			//	System.out.println("Response Code" + number);
			//	System.out.println("Response Parameter" + remaining);
			}
			// outputMap.put("Reuest Parameter", remaining);
			LinkedList<String> words = new LinkedList<String>();
			// outputMap.put("Commandline", line);
			Pattern p1 = Pattern.compile("[A-Z]+\\s");
			Matcher m1 = p1.matcher(remaining);
			while (m1.find()) {
				//System.out.println(m1.group());
				words.add(m1.group());
			}
		//	System.out.println(remaining);
			//System.out.println();
			if (words.size() > 0) {
				outputMap.put("Command", words.get(0));
				remaining = remaining.replace(words.get(0), "");
				if(remaining.trim().length() > 0)
				outputMap.put("Request Parameter:", remaining);
			} else {
				if(remaining.trim().length() > 0)
				outputMap.put("Response Parameter", remaining);
			}
		}

		return outputMap;
	}

	public static List<LinkedHashMap> verifySMTPPacket(String hexdump) throws CustomException {
		if (hexdump == null)
			throw new CustomException("Dont pass null");
		List<LinkedHashMap> smtplist = new LinkedList<LinkedHashMap>();
		// LinkedHashMap<String, String> outputMap = new LinkedHashMap();
		try {

			String text = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(hexdump), "UTF-8");
			String[] lines = (text.split("[\\r\\n]+"));
			// outputMap.put("list", list.toString());
			for (int i = 0; i < lines.length; i++)
				// outputMap.put(String.valueOf(i), lines[i]);
				smtplist.add(parseSMTPPacket(lines[i]));

		} catch (Exception e) {
			//e.printStackTrace();

		}
		return smtplist;
	}

//	public static void main(String args[]) throws CustomException {
//
//		 String hexDumpV4 = "515549540d0a";
//		//String hexDumpV4 = "35353320736F7272792C20746861742061646472657373206973206E6F7420696E206D79206C697374206F6620616C6C6F77656420726563697069656E7473202823352E372E31290D0A";
//		
//		List<LinkedHashMap> outv4 = verifySMTPPacket(hexDumpV4);
//
//		System.out.println("output is here : " + outv4);
//
//	}

}
