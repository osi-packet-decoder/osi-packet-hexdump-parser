package com.parser.service;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.parser.customError.CustomException;


public class FTPPacketVerifier {
	
	/**
	 * 
	 * @author Priti Kumari
	 * 
	 *         {@link} https://tools.ietf.org/html/rfc959
	 *
	 */

	private enum Command {
		ABOR,ACCT,ADAT,ALLO,APPE,AUTH,CCC,CDUP,CONF,CWD,DELE,ENC,EPRT,EPSV,FEAT,HELP,LANG,LIST,LPRT,LPSV,MDTM,MIC,MKD,MLSD,MLST,MODE,NLST,NOOP,OPTS,
		PASS,PASV,PBSZ,PORT,PROT,PWD,QUIT,REIN,REST,RETR,RMD,RNFR,RNTO,SITE,SIZE,SMNT,STAT,STOR,STOU,STRU,SYST,TYPE,USER,XCUP,XMKD,XPWD,XRCP,XRMD,
		XRSQ,XSEM,XSEN,CLNT;
	}
	
	private enum Attribute {
		RMR("Restart marker reply",110),
		SRM("Service ready in nnn minutes",120),
		DCAO("Data connection already open; transfer starting",125),
		FSO("File status okay; about to open data connection",150),
		COM("Command okay", 200),
		CIS("Command not implemented, superfluous at this site", 202),
		SSS("System status, or system help reply", 211),
		DIS("Directory status", 212),
		FIS("File status", 213),
		HM("Help message", 214),
		NST("NAME system type", 215),
		SRU("Service ready for new user", 220),
		SCCC("Service closing control connection", 221),
		DCO("Data connection open; no transfer in progress", 225),
		CDC("Closing data connection", 226),
		EPM("Entering Passive Mode <h1,h2,h3,h4,p1,p2>", 227),
		ELPM("Entering Long Passive Mode", 228),
		EPME("Extended Passive Mode Entered", 229),
		UPLP("User logged in, proceed", 230),
		RFAO("Requested file action okay, completed", 250),
		PC("\"PATHNAME\" created", 257),
		UNO("User name okay, need password", 331),
		NAL("Need account for login", 332),
		FAPFI("Requested file action pending further information", 350),
		SNACC("Service not available, closing control connection", 421),
		CODC("Can't open data connection", 425),
		CCTA("Connection closed; transfer aborted", 426),
		RFANT("Requested file action not taken", 450),
		RAALP("Requested action aborted. Local error in processing", 451),
		RANT("Requested action not taken", 452),
		SECU("Syntax error, command unrecognized", 500),
		SEPA("Syntax error in parameters or arguments", 501),
		CNI("Command not implemented", 502),
		BSC("Bad sequence of commands", 503),
		CNIP("Command not implemented for that parameter", 504),
		SAFA("Supported address families are <af1, .., afn>", 521),
		PNS("Protocol not supported", 522),
		NLI("Not logged in", 530),
		NASF("Need account for storing files", 532),
		RAT("Requested action not taken", 550),
		RAAPTU("Requested action aborted. Page type unknown", 551),
		RFAA("Requested file action aborted", 552),
		RA_NT("Requested action not taken", 553),
		RANTRP("Requested action not taken: invalid REST parameter", 554),
		RANTSM("Requested action not taken: type or stru mismatch", 555);
		
		
		private Attribute(String attributeName, int code) {
			this.attributeName = attributeName;
			this.code = code;
		}

		String attributeName;
		int code;



	}
	
	private static Set<String> attribute = new HashSet<String>();
	static {
		for (Attribute attributeObj : Attribute.values()) {
			attribute.add(attributeObj.name());
			attribute.add(attributeObj.attributeName);
			attribute.add(attributeObj.code+"");			
		}
	}
	
	private static Set<String> commands = new HashSet<String>();
	
	static {
		for (Command command : Command.values()) {
			commands.add(command.name());
		}
	}

	private static String checkForCommand(String line, LinkedHashMap<String, String> outputMap) {
		String[] splitBySpace = line.split(" ");
		String parsedString = new String();
		for (String term : splitBySpace) {
			if (commands.contains(term)) {
				outputMap.put("Request command", term);
			} else {
				parsedString += " " + term;
				//outputMap.put("Request command", parsedString);
			}
		}
		return parsedString.trim();
	}	
	public static LinkedHashMap<String, String> parseFTPPacket(String line) {
		
		LinkedHashMap<String, String> outputMap = new LinkedHashMap();
		try {
		outputMap.put("Line", line);
		//line = checkForCommand(line,outputMap);
		line=line.replace('-', ' ');
		String[] parts = (line.split("[' ']+"));
		//String[] partsByDash = (line.split("-"));
		Attribute getAttribute=  getAttribute(0);
		
		//System.out.println(parts[0]+"parts....partsByDash..."+partsByDash[0]);
		
		 if (parts.length >0) {	
			 if ( (parts[0].length() == 3 &&  parts[0].matches("[0-9]+"))   ) {
			//if ( (parts[0].length() == 3 &&  parts[0].matches("[0-9]+"))|| (partsByDash[0].length() == 3 &&  partsByDash[0].matches("[0-9]+"))   ) {
				//System.out.println("iff.."+partsByDash[0]);
				if(parts[0].length() == 3 &&  parts[0].matches("[0-9]+")) {
					 getAttribute = getAttribute(Integer.parseInt(parts[0]));							
				//	outputMap.put("Response code",  getAttribute.attributeName+"("+getAttribute.code+")");
				}
//				else if(partsByDash[0].length() == 3 &&  partsByDash[0].matches("[0-9]+")) {
//					 getAttribute = getAttribute(Integer.parseInt(partsByDash[0]));							
//						
//				}
				outputMap.put("Response code",  getAttribute.attributeName+"("+getAttribute.code+")");
		    String parsedString = "";
			//for (String term : parts) {
			if(line.length()>3 && parts[0].length()==3)
				parsedString +=  line.substring(parts[0].length());
			//}
//			else if(line.length()>3 && partsByDash[0].length()==0)
//				parsedString += line.substring(partsByDash[0].length());
			outputMap.put("Response arg", parsedString);
			
			if(getAttribute.code==227) {
				String[] lines = (parsedString.split("[\\r\\s]+"));
				//System.out.println(lines[lines.length-1]+"values.."+Arrays.toString(lines));
				String []values=lines[lines.length-1].split(",");
				String portVal2 = values[values.length-1].substring(0, values[values.length-1].length()-1);
				//System.out.println("portVal2.."+portVal2);
				String passiveIPAddress=values[0].substring(1)+"."+values[1]+"."+values[2]+"."+values[3];
				outputMap.put("Passive IP address", passiveIPAddress);
				outputMap.put("Passive port", ((Integer.parseInt(values[4])*256)+Integer.parseInt(portVal2))+"");
			}
			
			}else {
				//System.out.println(parts[0]+"...elsssssssssss  "+line);
				if(line.length() > 0) {
//					if(parts[0].equals(""))
//					outputMap.put("Request command", line);
//					else {
						if(parts[0].length()!=0) {
							outputMap.put("Request command", parts[0]);
							if(line.substring(parts[0].length()).length()>0)
							outputMap.put("Request arg", line.substring(parts[0].length()));
						}
					//}
				}
			}
		}
		
		}catch (Exception e) {
			outputMap.put("Malformed Packet", e.getMessage());
			//e.printStackTrace();
		}
		return outputMap;
	}
	
	private static Attribute getAttribute(int code) {
		for (Attribute e : Attribute.values()) {
			if (e.code == code ) {
				//return e.attributeName+"("+e.code+")";
				return e;
			}
		}
		return null;		
	}
	@SuppressWarnings("rawtypes")
	public static List<LinkedHashMap> verifyFTPPacket(String hexdump) throws CustomException {
		if (hexdump == null || hexdump.length()==1)
			throw new CustomException("Please enter valid hexdump");
		List<LinkedHashMap> FTPlist = new LinkedList<LinkedHashMap>();
		try {
			String text = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(hexdump), "UTF-8");
			String[] lines = (text.split("[\\r\\n]+"));
			for (int i = 0; i < lines.length; i++)
			FTPlist.add(parseFTPPacket(lines[i]));
		} catch (Exception e) {
		}
		return FTPlist;
	}

	public static void main(String args[]) throws CustomException {
		//String hexDumpV4="535953540D0A";
		//String hexDumpV4="52455452204D757369632E6D70330D0A";
		//String hexDumpV4 = "3233302055736572206373616E64657273206C6F6767656420696E2E0D0A";
		String hexDumpV4="3231312D457874656E73696F6E7320737570706F727465643A0D0A20434C4E540D0A204D44544D0D0A20504153560D0A20524553542053545245414D0D0A2053495A450D0A32313120456E642E0D0A";
		// String hexDumpV4 = "32313520554E495820547970653A204C380D0A";
				//String hexDumpV4 = "5342523520726573756D652E646F630D0A";
		//String hexDumpV4 ="313530204F70656E696E672042494E415259206D6F6465206461746120636F6E6E656374696F6E20666F7220524541444D450D0A";
			//	String hexDumpV4 = "3235302043574420636F6D6D616E64207375636365737366756C2E20222F222069732063757272656E74206469726563746F72792E0D0A";
			
		List<LinkedHashMap> outv4 = verifyFTPPacket(hexDumpV4);

		System.out.println("output is here : " + outv4);

	}
}