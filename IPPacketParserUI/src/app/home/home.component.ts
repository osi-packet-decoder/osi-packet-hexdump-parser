import { Component, OnInit ,ViewChild,ElementRef} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../_service/user.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import Swal from 'sweetalert2';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit {
  @ViewChild('userFile',{ static: true }) userFile: ElementRef;
  fileToUpload: File = null;
  inputForm: FormGroup;
  Data;
  uploadFileData: any;
  decodeData: any;
  outputBox = false;
  result: any = [];
  keys: any;
  nestedData: any;
  tcpDataKey: any;
  ipPacketData: any;
  TCPData: any;
  tcpDownloadData: any;
  HttpData;
  HttpDataKey;
  packetName: any;
  packetData:any;
  tcp = false;
  udp = false;
  http = false;
  singlePacket = false;
  multiplePacket = false;
  multipleFile;
  fileLength:any;
  downloadData:any;
  checkData = [];
  muliplePacketIp = [];
  hexString=[];
  hexDump: string;
  hexString2=[];  
  today:any;
  sctp: boolean;
  sctpDataKey: string[];
  SCTPData: {};
  uploadFileData1: File;
  

  constructor(private userService: UserService,private datePipe:DatePipe) {
    this.today = this.datePipe.transform( new Date(),'yyyy-MM-dd  h:mm:ss');
   }

  ngOnInit() {
    this.inputForm = new FormGroup({
      hexDump: new FormControl()
    })
  }




//   handleFileInput1(files: FileList) {
//     debugger
//     this.outputBox = false;
//     this.inputForm.reset();
//     this.outputBox = true;    
//     if (files && files[0]) {
//       console.log("file",files);
//       this.uploadFileData = files[0]; 
//       var reader = new FileReader();
//       reader.readAsText = (event2:any)=>{
// console.log("reader....."+event2);
//       }         
//       this.userService.fileUpload(this.uploadFileData).subscribe((res)=>{
//       console.log("check",res);      
//     })
//   }  //if closing
// }  

    //upload function
    
    handleFileInput(files: FileList) {
      //debugger
      this.outputBox = false;      
      this.inputForm.reset();
      this.outputBox = true;         
      // console.log("file",files);
      if (files && files[0]) {
        this.uploadFileData1 = files[0];  
        var reader = new FileReader();
        reader.onload = (event: any) => {          
          this.uploadFileData = event.target.result;
          //console.log("this.uploadFileData..."+JSON.stringify(this.uploadFileData))    
          this.Data = this.uploadFileData.split(",").pop();
          //console.log("this.Data"+atob(this.Data));
      //console.log( (atob(this.Data)));
      this.inputForm.setValue({
             hexDump: atob(this.Data)
             //hexDump:list          
     })
          var uploadData = {};
          var key = "file";

         // uploadData[key] = this.Data;
         uploadData[key] =  atob(this.Data);
           //console.log("key",uploadData)
          this.userService.fileUpload( this.uploadFileData1).subscribe((res) => {

            let data:any= res;
            if (res == null || data.errorMessage) {
              Swal.fire('Oops...', 'Invalid Packet!', 'error');
            } else { 
              this.tcpDownloadData=res;
              this.packetData = res;
             // console.log("result",this.packetData)
             this.multipleFile = res;
             this.downloadData = res;
             this.fileLength=this.multipleFile.length
            
              Swal.fire('Congratulations', 'Your result is here!', 'success');
              if(this.fileLength) {
              for( var i=0;i < this.fileLength; i++) {             
              
                this.singlePacket = false;
                this.multiplePacket = true;
                //this.inputForm.reset();
               // this.addMultpleFile(i,this.multipleFile[i]);              
                // console.log("check mutiple",this.multipleFile[0])
              }
              }
              else {
              this.addToBox(res);
              this.multiplePacket = false;
              this.singlePacket = true;
              }
            }
          }, (error: HttpErrorResponse) => {
            // console.log("abc",error.error.status)
            Swal.fire('Oops...', 'Invalid Packet!', 'error');
            this.outputBox = false;
          }

          )
        }

        reader.readAsDataURL(files[0]);
        this.clearSelectedFile();
      }


    }


  clearSelectedFile() {
    this.userFile.nativeElement.value = null;
    }

  decode(event: any) {
    this.outputBox = false;
    this.outputBox = true;
    
    //this.singlePacket.reset();
    //this.outputBox

    //To remove white space between string
    // this.decodeData = event.replace(/\s/g, "");
    this.decodeData = JSON.parse(JSON.stringify(event));
    // console.log("this.decodeData.."+JSON.stringify(this.decodeData));
    this.userService.decoder(this.decodeData).subscribe((res: any) => {
      // console.log("res",res)
      if (res == null || res.errorMessage) {
        this.outputBox = false;
        Swal.fire('Oops...', 'Invalid Packet!', 'error')
      } else {
        Swal.fire('Congratulations', 'Your result is here!', 'success')
        this.result = res;
        //console.log(this.result.length)
        if( this.result.length==undefined){
          //console.log("ifffff")
          this.multiplePacket = false;
          this.addToBox(this.result);
        }else{
         // console.log("else  "+ this.result)
          this.tcpDownloadData=res;
            this.packetData = res;
            //console.log("result",this.packetData)
           this.multipleFile = res;
           this.downloadData = res;
           this.fileLength=this.multipleFile.length
            for( var i=0;i < this.fileLength; i++) {             
             
              this.singlePacket = false;
              this.multiplePacket = true;
              //this.inputForm.reset();
             // this.addMultpleFile(i,this.multipleFile[i]);
              // console.log("check mutiple",this.multipleFile[0])
            }            
        }
      }

    },
      (error: HttpErrorResponse) => {
        // console.log("abc",error.error.status)
        Swal.fire('Oops...', 'Invalid Packet!', 'error');
        this.outputBox = false;
      }
    )
  }

  addToBox(data) {
    //debugger;
    //this.inputForm.reset();
    this.outputBox=false;
    this.outputBox=true;
    this.singlePacket=true;
    this.downloadData = data;
    this.packetData = data["IP Packet 1"];
    //console.log("mutlrr",this.packetData)
    const prop = 'Hexdump';
    this.packetData = Object.keys(this.packetData).reduce((object, key) => {
      if (key !== prop) {
        object[key] = this.packetData[key]
      }
      return object
    }, {});
    // this.keys = Object.keys(this.ipPacketData);
    
    // this.ipPacketData = this.packetData;
    // this.keys = Object.keys(this.ipPacketData);

    // console.log("check packet data",this.packetData,"tcp",this.packetData.TCP)
    // console.log("fdsfsdf",data);
    // debugger;
    if (this.packetData.TCP) {
      this.outputBox = true;
      // console.log("check packet",this.packetData.TCP)
      this.tcp = true;
      this.udp = false;
      this.sctp = false;
      this.nestedData = this.packetData.TCP;
      // console.log("this nested",this.packetData.TCP)
      // delete this.packetData.TCP;

      const prop = 'TCP';
      this.ipPacketData = Object.keys(this.packetData).reduce((object, key) => {
        if (key !== prop) {
          object[key] = this.packetData[key]
        }
        return object
      }, {});

      // console.log("asdsafsafwr",this.ipPacketData)
      this.keys = Object.keys(this.ipPacketData);
      // console.log("check key before",this.keys)

      if(this.nestedData.HTTP) {
        this.http = true;
        this.HttpData = this.nestedData.HTTP;
        this.HttpDataKey = Object.keys(this.HttpData);
  
        // delete this.nestedData.HTTP;
        const prop1 = 'HTTP';
        this.TCPData = Object.keys(this.packetData.TCP).reduce((object, key) => {
          if (key !== prop1) {
            object[key] = this.packetData.TCP[key]
          }
          return object
        }, {});
  
        this.tcpDataKey = Object.keys(this.TCPData);
        // console.log("tcpDataKey",this.tcpDataKey)
      }
      else {


        this.sctp = false;
        this.http = false;
        this.TCPData = this.nestedData;
        this.tcpDataKey = Object.keys(this.TCPData);
      }
    }
    
    else if (this.packetData.UDP) {
      this.outputBox = true;
      this.sctp = false;
      this.udp = true;
      this.tcp = false;
      this.nestedData = this.packetData.UDP;      
      // delete this.ipPacketData.UDP;
      const prop3 = 'UDP';
      this.ipPacketData = Object.keys(this.packetData).reduce((object, key) => {
        if (key !== prop3) {
          object[key] = this.packetData[key]
        }
        return object
      }, {});

      
      this.keys = Object.keys(this.ipPacketData);
    
      if(this.nestedData.HTTP) {
        this.http = true;
        this.HttpData = this.nestedData.HTTP;
        this.HttpDataKey = Object.keys(this.HttpData);
        // delete this.nestedData.HTTP;
        const prop4 = 'HTTP';
        this.TCPData = Object.keys(this.packetData.UDP).reduce((object, key) => {
          if (key !== prop4) {
            object[key] = this.packetData.UDP[key]
          }
          return object
        }, {});
        // this.TCPData = this.nestedData;
        this.tcpDataKey = Object.keys(this.TCPData);

        
      } else {
        this.http = false;
        this.TCPData = this.nestedData;
        this.tcpDataKey = Object.keys(this.TCPData);
      }



    }else if (this.packetData.SCTP) {
     // this.outputBox = true;
      this.sctp = true;
      this.udp = false;
      this.tcp = false;
      this.nestedData = this.packetData.SCTP;    
      // delete this.ipPacketData.UDP;
      const prop3 = 'SCTP';
      this.ipPacketData = Object.keys(this.packetData).reduce((object, key) => {
        if (key !== prop3) {
          object[key] = this.packetData[key]
        }
        return object
      }, {});
      this.keys = Object.keys(this.ipPacketData);
      this.SCTPData = this.nestedData;
      this.sctpDataKey = Object.keys(this.SCTPData);
      
    }
    else {
      this.outputBox = true;
      this.ipPacketData = this.packetData;
      this.sctp = false;
      this.udp = false;
      this.tcp = false;
      this.keys = Object.keys(this.ipPacketData);
    }

  }

  addMultpleFile(i,data) {
    this.outputBox=true;   
    this.packetData[i] = data;
    // debugger;
  //  delete data.hexDump;
    const prop = 'Hexdump';
    this.packetData[i] = Object.keys(data).reduce((object, key) => {
      if (key !== prop) {
        object[key] = data[key]
      }
      return object
    }, {});
   
// console.log("show",this.downloadData);

  }
  unsorted(): any { 
  }
  downloadJson() {
    this.today = this.datePipe.transform( new Date(),'yyyy-MM-dd_hh-mm-ss');
    var jsonData = this.downloadData;
   // console.log("jsonData..."+JSON.stringify(jsonData));
    var customName="";
    if(jsonData.SCTP!=undefined){
      
      customName="IPv"+version+"-SCTP";
    }else if(jsonData.UDP!=undefined){
      
      if( jsonData.UDP.DTLS !=undefined)
        customName="IPv"+version+"-UDP-"+"DTLS";
        else if( jsonData.UDP.SIP !=undefined)
        customName="IPv"+version+"-UDP-"+"SIP";
        else
        customName="IPv"+version+"-UDP";
    }else if(jsonData.TCP!=undefined){
     
      if( jsonData.TCP.TLS !=undefined)
        customName="IPv"+version+"-TCP-"+"TLS";
        else if( jsonData.TCP.SMTP !=undefined)
        customName="IPv"+version+"-TCP-"+"SMTP";
        else if( jsonData.TCP.SIP !=undefined)
        customName="IPv"+version+"-TCP-"+"SIP";
        else if( jsonData.TCP.HTTP !=undefined)
        customName="IPv"+version+"-TCP-"+"HTTP";
        else
        customName="IPv"+version+"-TCP";
    }
    else if(jsonData.length==1)    {
      var version="";
      
    for (let index = 0; index < this.downloadData.length; index++) {
     version=this.downloadData[index]["IP Packet 1"].version;
     jsonData= this.downloadData[index]["IP Packet 1"];
    // console.log("jjjjj.."+jsonData)
        if(jsonData.SCTP!=undefined){      
        customName="IPv"+version+"-SCTP";
      }else if(jsonData.UDP!=undefined){
        
        if( jsonData.UDP.DTLS !=undefined)
          customName="IPv"+version+"-UDP-"+"DTLS";
          else if( jsonData.UDP.SIP !=undefined)
          customName="IPv"+version+"-UDP-"+"SIP";
          else
          customName="IPv"+version+"-UDP";
      }else if(jsonData.TCP!=undefined){
       
          if( jsonData.TCP.TLS !=undefined)
          customName="IPv"+version+"-TCP-"+"TLS";
          else if( jsonData.TCP.SMTP !=undefined)
          customName="IPv"+version+"-TCP-"+"SMTP";
          else if( jsonData.TCP.SIP !=undefined)
          customName="IPv"+version+"-TCP-"+"SIP";
          else if( jsonData.TCP.HTTP !=undefined)
          customName="IPv"+version+"-TCP-"+"HTTP";
          else if( jsonData.TCP['Diameter Protocol'] !=undefined)
          customName="IPv"+version+"-TCP-"+"Diameter Protocol";
          else if( jsonData.TCP.FTP !=undefined)
          customName="IPv"+version+"-TCP-"+"FTP";
          else
          customName="IPv"+version+"-TCP";

      }

     //console.log("customName....."+customName);
    }

      //customName="IP";
    }      
     else
        customName="IP-HexDecode";
      

    var file = JSON.stringify(this.downloadData, null, '\t');
    var element = document.createElement('a');
    element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(file));
    element.setAttribute('download', customName+"("+this.today+")"+".json");
    element.style.display = 'grid';
    document.body.appendChild(element);
    element.click(); // simulate click   
    document.body.removeChild(element);

  }

  
}
