import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { map } from "rxjs/operators";
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class UserService {

//decoderUrl = "http://203.122.22.90:8087/";
decoderUrl = "http://localhost:8087/";
  baseUrl: any;

//decoderUrl = "http://34.70.64.223:8087/";


  constructor( private http : HttpClient) { }

  decoder(hexDump:any):any {

   return this.http.post(this.decoderUrl + 'ipPacketParser',hexDump);
  }  
  // fileUpload(fileString:any) {
  //   return this.http.post(this.decoderUrl + 'uploadBulkIpPackets',fileString);
  // }


  fileUpload(fileString:any) {
    const data = new FormData();
    data.append('file',fileString)
    
    return this.http.post(this.decoderUrl + 'uploadBulkIpPackets',data);
    }

 

}
