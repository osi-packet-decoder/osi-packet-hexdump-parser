// const { app, BrowserWindow, Menu } = require('electron')
// const path = require('path')
// const url = require('url')
// const fs = require('fs')


// function createWindow() {
 
//     // Create the browser window.
//     const win = new BrowserWindow({
//         width: 800,
//         height: 600,
//         webPreferences: {
//             nodeIntegration: true
//         }
//     })

//     // and load the index.html of the app.
//     win.loadFile('src/index.html')

//     // Open the DevTools.
//     win.webContents.openDevTools()

//     // To show Menu
//     var menu = Menu.buildFromTemplate([
//         {
//             label: 'File',
//             submenu: [
//                 {
//                     label: 'Exit',
    
//                     click() { 
//                         app.quit() 
//                     }
//                 }
//             ]
//         }
//     ])
    
//     Menu.setApplicationMenu(menu)

// }


// // This method will be called when Electron has finished
// // initialization and is ready to create browser windows.
// // Some APIs can only be used after this event occurs.
// app.whenReady().then(createWindow)

// // Quit when all windows are closed.
// app.on('window-all-closed', () => {
//     // On macOS it is common for applications and their menu bar
//     // to stay active until the user quits explicitly with Cmd + Q
//     if (process.platform !== 'darwin') {
//         app.quit()
//     }
// })

// app.on('activate', () => {
//     // On macOS it's common to re-create a window in the app when the
//     // dock icon is clicked and there are no other windows open.
//     if (BrowserWindow.getAllWindows().length === 0) {
//         createWindow()
//     }
// })

// // In this file you can include the rest of your app's specific main process
// // code. You can also put them in separate files and require them here.

const {app, BrowserWindow} = require('electron')
    const url = require("url");
    const path = require("path");

    let mainWindow

    function createWindow () {
      mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
          nodeIntegration: true
        }
      })

      mainWindow.loadURL(
        url.format({
          pathname: path.join(__dirname, `../dist/protocol/index.html`),
          protocol: "file:",
          slashes: true
        })
      );
      // Open the DevTools.
      mainWindow.webContents.openDevTools()

      mainWindow.on('closed', function () {
        mainWindow = null
      })
    }

    app.on('ready', createWindow)

    app.on('window-all-closed', function () {
      if (process.platform !== 'darwin') app.quit()
    })

    app.on('activate', function () {
      if (mainWindow === null) createWindow()
    })