

# OSI Packet Parser #

### Clone ###

git clone https://bitbucket.org/osi-packet-decoder/osi-packet-hexdump-parser/src/master/


### Branches and their Purpose ###

##### master #####

Main Release Branch

`git checkout master`

##### develop #####

Stable Development / QA Branch

`git checkout develop`

##### any-feature #####

Feature Branch

`git checkout my-feature-branch`


### Other Commands ###

Checkout a new branch in git

`git checkout -b new-branch`

Add Changes in git

`git add *`

Commit Changes in git

`git commit -m "[feature] description"`

Push Changes to remote git

`git push -u origin new-branch`




